CREATE DATABASE  IF NOT EXISTS `empresa` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `empresa`;
-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: empresa
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `id_cliente` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `apellido` varchar(50) DEFAULT NULL,
  `cedula` int DEFAULT NULL,
  `direccion` varchar(50) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `correo` varchar(35) DEFAULT NULL,
  `est_cli` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES (25,'Richard','Torres',1105673816,'Borja','0','sf@sfv','A'),(26,'ronny','contento',1105864621,'chontacruz','0967264403','happy.m.a.k@hotmail.com','A');
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compra`
--

DROP TABLE IF EXISTS `compra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `compra` (
  `id_compra` int NOT NULL,
  `id_emp` int DEFAULT NULL,
  `num_fact` int DEFAULT NULL,
  `fecha_emision` date DEFAULT NULL,
  `total` double DEFAULT NULL,
  PRIMARY KEY (`id_compra`),
  KEY `id_compra` (`id_compra`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compra`
--

LOCK TABLES `compra` WRITE;
/*!40000 ALTER TABLE `compra` DISABLE KEYS */;
INSERT INTO `compra` VALUES (1,5,1,'2018-07-26',0.25),(2,5,2,'2018-07-26',10),(3,5,3,'2018-07-26',5846),(4,5,4,'2018-07-26',125),(5,5,5,'2018-07-26',2300);
/*!40000 ALTER TABLE `compra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_factura`
--

DROP TABLE IF EXISTS `detalle_factura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `detalle_factura` (
  `id_detalle` int NOT NULL AUTO_INCREMENT,
  `num_factura` varchar(10) DEFAULT NULL,
  `cantidad` int DEFAULT NULL,
  `des_fact` varchar(50) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  PRIMARY KEY (`id_detalle`),
  KEY `num_factura` (`num_factura`),
  KEY `id_lote` (`des_fact`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_factura`
--

LOCK TABLES `detalle_factura` WRITE;
/*!40000 ALTER TABLE `detalle_factura` DISABLE KEYS */;
INSERT INTO `detalle_factura` VALUES (36,'000000001',1,'FINALIN',0.5,0.5),(37,'000000001',1,'PARACETAMOL',2.25,2.25),(38,'000000001',1,'LORATADINA',1.5,1.5),(39,'000000002',1,'PARACETAMOL',2.25,2.25),(40,'000000002',1,'LORATADINA',1.5,1.5),(41,'000000002',1,'FINALIN',0.5,0.5),(42,'000000003',1,'PARACETAMOL',2.25,2.25),(43,'000000003',1,'LORATADINA',1.5,1.5),(44,'000000003',1,'FINALIN',0.5,0.5);
/*!40000 ALTER TABLE `detalle_factura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factura`
--

DROP TABLE IF EXISTS `factura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `factura` (
  `num_factura` varchar(10) NOT NULL,
  `fecha` date NOT NULL,
  `id_cliente` int DEFAULT NULL,
  `subtotal` double DEFAULT NULL,
  `IVA` double DEFAULT NULL,
  `total` double NOT NULL,
  `id_ger` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`num_factura`),
  KEY `id_cliente` (`id_cliente`),
  KEY `id_administrador` (`id_ger`),
  CONSTRAINT `factura_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id_cliente`),
  CONSTRAINT `factura_ibfk_2` FOREIGN KEY (`num_factura`) REFERENCES `detalle_factura` (`num_factura`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factura`
--

LOCK TABLES `factura` WRITE;
/*!40000 ALTER TABLE `factura` DISABLE KEYS */;
INSERT INTO `factura` VALUES ('000000001','2018-07-26',26,4.25,0.51,4.76,'A001'),('000000002','2018-07-26',26,4.25,0.51,4.76,'A001'),('000000003','2018-07-26',26,4.25,0.51,4.76,'A001');
/*!40000 ALTER TABLE `factura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gerente`
--

DROP TABLE IF EXISTS `gerente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `gerente` (
  `id_ger` varchar(10) NOT NULL,
  `nombreG` varchar(20) DEFAULT NULL,
  `ape_ger` varchar(20) DEFAULT NULL,
  `telefono` int DEFAULT NULL,
  `cedula` int DEFAULT NULL,
  `contraseña` varchar(10) DEFAULT NULL,
  `est_ad` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id_ger`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gerente`
--

LOCK TABLES `gerente` WRITE;
/*!40000 ALTER TABLE `gerente` DISABLE KEYS */;
INSERT INTO `gerente` VALUES ('A001','ronny','contento',967264403,1105864621,'123','A');
/*!40000 ALTER TABLE `gerente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lote`
--

DROP TABLE IF EXISTS `lote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `lote` (
  `id_lote` varchar(10) NOT NULL,
  `fecha_elaboracion` date DEFAULT NULL,
  `fecha_caducidad` date DEFAULT NULL,
  `id_medicamento` int DEFAULT NULL,
  PRIMARY KEY (`id_lote`),
  KEY `id_medicamento` (`id_medicamento`),
  CONSTRAINT `lote_ibfk_1` FOREIGN KEY (`id_medicamento`) REFERENCES `medicamento` (`id_med`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lote`
--

LOCK TABLES `lote` WRITE;
/*!40000 ALTER TABLE `lote` DISABLE KEYS */;
INSERT INTO `lote` VALUES ('1','2018-07-14','2018-08-26',13),('123','2018-07-14','2018-08-26',10),('1234','2018-07-14','2018-08-26',12),('2','2018-07-14','2018-08-26',11),('432','2018-07-14','2018-08-26',14);
/*!40000 ALTER TABLE `lote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lote_compra`
--

DROP TABLE IF EXISTS `lote_compra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `lote_compra` (
  `id_lotcom` int NOT NULL AUTO_INCREMENT,
  `id_compra` int DEFAULT NULL,
  `cod_lote` varchar(10) DEFAULT NULL,
  `cantidad` int DEFAULT NULL,
  `precio` double DEFAULT NULL,
  PRIMARY KEY (`id_lotcom`),
  KEY `id_compra` (`id_compra`),
  CONSTRAINT `lote_compra_ibfk_1` FOREIGN KEY (`id_compra`) REFERENCES `compra` (`id_compra`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lote_compra`
--

LOCK TABLES `lote_compra` WRITE;
/*!40000 ALTER TABLE `lote_compra` DISABLE KEYS */;
INSERT INTO `lote_compra` VALUES (9,1,'123',1,0.25),(10,2,'123',10,1),(11,3,'123',100,1.23),(12,3,'1234',100,23),(13,3,'1',100,34),(14,3,'2',1,23),(15,4,'432',100,1.25),(16,5,'2',100,23);
/*!40000 ALTER TABLE `lote_compra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medicamento`
--

DROP TABLE IF EXISTS `medicamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `medicamento` (
  `id_med` int NOT NULL AUTO_INCREMENT,
  `stock` int DEFAULT NULL,
  `nombre_med` varchar(50) DEFAULT NULL,
  `tipo_med` varchar(50) DEFAULT NULL,
  `precio_venta` double DEFAULT NULL,
  `precio_compra` double DEFAULT NULL,
  `est_med` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id_med`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medicamento`
--

LOCK TABLES `medicamento` WRITE;
/*!40000 ALTER TABLE `medicamento` DISABLE KEYS */;
INSERT INTO `medicamento` VALUES (10,67,'FINALIN','Cápsula',0.5,1.23,'P'),(11,100,'REXONA','Aerosol',24,23,'A'),(12,67,'LORATADINA','Jarabe',24,23,'A'),(13,67,'PARACETAMOL','Jarabe',35,34,'A'),(14,100,'SINGRIPAL','Aerosol',1.3,1.25,'A');
/*!40000 ALTER TABLE `medicamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedor`
--

DROP TABLE IF EXISTS `proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `proveedor` (
  `id_emp` int NOT NULL AUTO_INCREMENT,
  `nom_emp` varchar(40) DEFAULT NULL,
  `tel_emp` varchar(15) DEFAULT NULL,
  `ruc_emp` varchar(15) DEFAULT NULL,
  `dir_emp` varchar(40) DEFAULT NULL,
  `cor_emp` varchar(40) DEFAULT NULL,
  `representante` varchar(30) DEFAULT NULL,
  `est_emp` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id_emp`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedor`
--

LOCK TABLES `proveedor` WRITE;
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;
INSERT INTO `proveedor` VALUES (5,'Fabril','','1105671380001','Belen','sfs@fsgsfg','Franco','A');
/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-11 14:58:21
