﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Farmacia.Controlador;
using Farmacia.Modelo;

namespace Farmacia.Vista
{
    public partial class frmproveedores : Form
    {
        public frmproveedores()
        {
            InitializeComponent();
        }

        int fila = -1;//Variable para saber que dato de la datagridview ha sido elegido
        string numced = "";//Variable que guarda el numero de cedula
        //Evento que abre la pantalla Registrar proveedor estado nuevo
        private void btnregistrar_Click(object sender, EventArgs e)
        {
            frmregistrarproveedor frmprov = new frmregistrarproveedor("");
            this.Close();
            frmprov.ShowDialog();
        }
        //Evento que manda a un metodo a llenar la informacion en la datagridview
        private void frmproveedores_Load(object sender, EventArgs e)
        {
            llenadatos("A");
        }
        //Metodo que llena la informacion en la datagridview
        public void llenadatos(string est)
        {
            try
            {
                dgvinfo.Rows.Clear();
                proveedordb objE = new proveedordb();
                if (chkdes.Checked == true)
                {
                    objE.getemp().Listaempresa = objE.traedatos("P");
                }
                else
                {
                    objE.getemp().Listaempresa = objE.traedatos(est);
                }
                if (objE.getemp().Listaempresa.Count == 0)
                {
                    MessageBox.Show("No hay registros");
                    txtbuscar.Enabled = false;
                    btndesactivar.Enabled = false;
                    btnmodificar.Enabled = false;
                }
                else
                {
                    fila = 0;
                    for (int i = 0; i < objE.getemp().Listaempresa.Count; i++)
                    {
                        dgvinfo.Rows.Add(1);
                        dgvinfo.Rows[i].Cells[0].Value = objE.getemp().Listaempresa[i].Ruc_emp;
                        dgvinfo.Rows[i].Cells[1].Value = objE.getemp().Listaempresa[i].Nom_emp;
                        dgvinfo.Rows[i].Cells[2].Value = objE.getemp().Listaempresa[i].Dir_emp;
                        dgvinfo.Rows[i].Cells[3].Value = objE.getemp().Listaempresa[i].Tel_emp;
                        dgvinfo.Rows[i].Cells[4].Value = objE.getemp().Listaempresa[i].Rep_emp;
                    }
                    txtbuscar.Enabled = true;
                    btndesactivar.Enabled = true;
                    btnmodificar.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }
        }
        //Evento que elije que fila se ha escogido de la datagridview
        private void dgvinfo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            fila = dgvinfo.CurrentRow.Index;
        }
        //Evento que abre la pantalla Registrar proveedor estado modificar
        private void btnmodificar_Click(object sender, EventArgs e)
        {
            numced = dgvinfo.Rows[fila].Cells[0].Value.ToString();
            frmregistrarproveedor frmrcprov = new frmregistrarproveedor(numced);
            this.Close();
            frmrcprov.ShowDialog();
        }
        //Evento que envia a un metodo para desactivar el proveedor
        private void btndesactivar_Click(object sender, EventArgs e)
        {
            desactivar();
        }
        //metodo para desactivar un proveedor
        private void desactivar()
        {
            try
            {
                proveedordb objC = new proveedordb();
                int resp;
                string mensaje = "";
                string ced = dgvinfo.Rows[fila].Cells[0].Value.ToString();
                if (MessageBox.Show("Desea " + btndesactivar.Text + " a " + dgvinfo.Rows[fila].Cells[1].Value.ToString(), "Empresa", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    if (btndesactivar.Text.Equals("Desactivar"))
                    {
                        resp = objC.desactivaemp(ced, "P");
                        mensaje = "Desactivado";
                    }
                    else
                    {
                        resp = objC.desactivaemp(ced, "A");
                        mensaje = "Activado";
                        chkdes.Checked = false;
                    }
                    if (resp > 0)
                    {
                        MessageBox.Show("Proveedor " + mensaje);
                        llenadatos("A");
                    }
                    else
                    {
                        MessageBox.Show("No se elimino proveedor ");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex.Message);
            }
        }
        //Evento que verifica si se deben mostrar los proveedores desactivados o activos
        private void chkdes_CheckedChanged(object sender, EventArgs e)
        {
            if (chkdes.Checked == true)
            {
                llenadatos("P");
                btndesactivar.Text = "Activar";
                btnregistrar.Enabled = false;
                btnmodificar.Enabled = false;
            }
            else
            {
                btnmodificar.Enabled = true;
                btndesactivar.Enabled = true;
                btnregistrar.Enabled = true;
                llenadatos("A");
                btndesactivar.Text = "Desactivar";
            }
        }
        //Evento que verifica el RUC y envia informacion a un metodo para la busqueda del proveedor
        private void btnbuscar_Click(object sender, EventArgs e)
        {
            validaced();            
        }

        private void validaced()
        {
            string res = "";
            if (txtbuscar.Text != "")
            {
                if (txtbuscar.TextLength == 13)
                {
                    string ce = txtbuscar.Text.ToString();
                    char[] vectorc = ce.ToArray();
                    ce = null;
                    for (int i = 0; i < 10; i++)
                    {
                        ce += vectorc[i].ToString();
                    }
                    res = util.validacedula(ce);
                    if (res == "Invalido")
                    {
                        MessageBox.Show("El numero de RUC es incorrecto");
                        txtbuscar.Focus();
                    }
                    else
                    {
                        if (chkdes.Checked == true)
                        {
                            buscar(txtbuscar.Text, "P");
                        }
                        else
                        {
                            buscar(txtbuscar.Text, "A");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Verifique si esta bien ingresado el RUC");
                    txtbuscar.Focus();
                }
            }        
        }
        //Metodo de busqueda del proveedor
        private void buscar(string txt, string est)
        {
            try
            {
                dgvinfo.Rows.Clear();
                proveedordb objE = new proveedordb();
                objE.getemp().Listaempresa = objE.buscaemp(txt, est);
                if (objE.getemp().Listaempresa.Count > 0)
                {
                    for (int i = 0; i < objE.getemp().Listaempresa.Count; i++)
                    {
                        dgvinfo.Rows.Add(1);
                        dgvinfo.Rows[i].Cells[0].Value = objE.getemp().Listaempresa[i].Ruc_emp;
                        dgvinfo.Rows[i].Cells[1].Value = objE.getemp().Listaempresa[i].Nom_emp;
                        dgvinfo.Rows[i].Cells[2].Value = objE.getemp().Listaempresa[i].Dir_emp;
                        dgvinfo.Rows[i].Cells[3].Value = objE.getemp().Listaempresa[i].Tel_emp;
                        dgvinfo.Rows[i].Cells[4].Value = objE.getemp().Listaempresa[i].Rep_emp;
                    }
                }
                else
                {
                    MessageBox.Show("No existe en el registro\nVerifique que el RUC sea el correcto","Empresa",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    txtbuscar.Text = null;
                    llenadatos(est);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error de busqueda " + ex.Message);
            }
        }
        //Evento para salir de la pantalla
        private void btnsalir_Click(object sender, EventArgs e)
        {
            util.mustrapantallas("P", 1);
            this.Close();
        }
        //Evento que valida si se habilita o no el boton [Buscar]
        private void txtbuscar_TextChanged(object sender, EventArgs e)
        {            
            if (txtbuscar.Text != "")
            {
                btnbuscar.Enabled = true;
            }
            else
            {
                if (chkdes.Checked == true)
                {
                    llenadatos("P");
                }
                else
                {
                    llenadatos("A");
                }
                btnbuscar.Enabled = false;
            }
        }
        //Evento que valida que solo se escriban numeros
        private void txtbuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            char letra = e.KeyChar;
            if (!char.IsDigit(letra) && letra != 13 && letra != 8)
                e.Handled = true;
        }

        private void frmproveedores_FormClosed(object sender, FormClosedEventArgs e)
        {
            util.mustrapantallas("P", 1);
        }
    }
}
