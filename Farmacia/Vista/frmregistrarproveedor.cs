﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Farmacia.Controlador;

namespace Farmacia.Vista
{
    public partial class frmregistrarproveedor : Form
    {
        public frmregistrarproveedor(string ruc)
        {
            InitializeComponent();
            this.ruc = ruc;
        }

        string ruc;//Variable que guarda el RUC
        string estado, res = "";//Variable estado guarda si en nuevo proveedor o lo va a modificar, y resp guarda la informacion de la cedula(valido, invalido)
        //Evento que verifica si el estado sera nuevo o a modificar
        private void frmregistrarproveedor_Load(object sender, EventArgs e)
        {
            if (ruc == "")
            {
                estado = "N";
            }
            if (ruc != "")
            {
                estado = "E";
                modificar(ruc);
            }   
        }
        //Metodo que trae los datos del proveedor para modificarlos
        private void modificar(string ced)
        {
            try
            {
                proveedordb objE = new proveedordb();
                objE.setemp(objE.traeemp(ced.ToString()));
                if (objE.getemp().Ruc_emp.ToString() == "")
                {
                    MessageBox.Show("No existe proveedor");
                }
                else
                {
                    txtruc.Text = objE.getemp().Ruc_emp.ToString();
                    txtnom.Text = objE.getemp().Nom_emp;
                    txtdir.Text = objE.getemp().Dir_emp;
                    txtcor.Text = objE.getemp().Cor_emp;
                    txttel.Text = objE.getemp().Tel_emp.ToString();
                    txtrep.Text = objE.getemp().Rep_emp;
                    txtruc.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en los datos, " + ex.Message);
            }
        }
        //Evento que manda a guardar o modificar la informacion
        private void btnguardar_Click(object sender, EventArgs e)
        {
            if (res == "Valido")
            {
                if (txtruc.Text != "" && txtnom.Text != "" && txtdir.Text != "" && txtrep.Text != "")
                {
                    if (estado == "N")
                    {
                        adicciona();
                    }
                    if (estado == "E")
                    {
                        editar();
                    }
                    frmproveedores frmprov = new frmproveedores();
                    this.Close();
                    frmprov.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Faltan datos");
                }
            }
            else
            {
                MessageBox.Show("Faltan datos");
                valida();
            }
        }
        //Metodo que valida que informacion no ha sido ingresada
        private void valida()
        {
            if (txtruc.Text == "")
            {
                error.SetError(txtruc, "Dato obligatorio");
            }
            if (txtnom.Text == "")
            {
                error.SetError(txtnom, "Dato obligatorio");
            }
            if (txtdir.Text == "")
            {
                error.SetError(txtdir, "Dato obligatorio");
            }
            if (txtrep.Text == "")
            {
                error.SetError(txtrep, "Dato obligatorio");
            }
        }
        //Metodo que envia la informacion al Modelo.proveedor para guardarla
        private void adicciona()
        {
            try
            {
                proveedordb objE = new proveedordb();
                int resp;
                llenapro(objE);
                resp = objE.insertaemp(objE.getemp());
                if (resp == 0)
                {
                    MessageBox.Show("No se han ingresado datos");
                }
                else
                {
                    MessageBox.Show("Proveedor ingresado");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error  " + ex.Message);
            }
        }
        //Metodo que llena la informacion en la clase proveedordb
        private proveedordb llenapro(proveedordb emp)
        {
            emp.getemp().Nom_emp = txtnom.Text;
            emp.getemp().Tel_emp = txttel.Text;
            emp.getemp().Ruc_emp = txtruc.Text;
            emp.getemp().Dir_emp = txtdir.Text;
            emp.getemp().Cor_emp = txtcor.Text;
            emp.getemp().Rep_emp = txtrep.Text;
            emp.getemp().Est_emp = "A";
            return emp;
        }
        //Metodo que envia la informacion al Modelo.proveedor para modificarla
        private void editar()
        {
            try
            {
                proveedordb objE = new proveedordb();
                int resp;
                llenapro(objE);
                resp = objE.Actualizaemp(objE.getemp());
                if (resp == 0)
                {
                    MessageBox.Show("No se modifico proveedor");
                }
                else
                {
                    MessageBox.Show("Proveedor modificado");
                    estado = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }
        }
        //Evento que valida el texbox de cdula al momento de su salida y verifica que sea correcta
        private void txtruc_Leave(object sender, EventArgs e)
        {
            validaced();
        }

        private void validaced()
        {
            if (txtruc.Text != "")
            {
                if (txtruc.TextLength == 13)
                {
                    string ce = txtruc.Text.ToString();
                    char[] vectorc = ce.ToArray();
                    ce = null;
                    for (int i = 0; i < 10; i++)
                    {
                        ce += vectorc[i].ToString();
                    }
                    res = util.validacedula(ce);
                    if (res == "Invalido")
                    {
                        MessageBox.Show("El RUC es incorrecto");
                        txtruc.Focus();
                    }
                    if (txtruc.Text != "")
                    {
                        buscar(txtruc.Text);
                    }
                    else
                    {
                        error.SetError(txtruc, "El RUC es obligatorio");
                    }
                }
                else
                {
                    MessageBox.Show("Verifique si esta bien ingresado el RUC");
                    txtruc.Focus();
                }
            }
            else
            {
                error.SetError(txtruc, "El numero de cedula es obligatorio");
            }
        }
        //Metodo para la busqueda del proveedor mediante el RUC
        private void buscar(string txt)
        {
            try
            {
                proveedordb objE = new proveedordb();
                objE.getemp().Listaempresa = objE.buscaemp(txt, "T");
                if (objE.getemp().Listaempresa.Count > 0)
                {
                    string es =objE.getemp().Listaempresa[0].Est_emp;
                    if (es == "A")
                    {
                        MessageBox.Show("El proveedor ya esta registrado");
                    }
                    else
                    {
                        MessageBox.Show("El proveedor ya esta registrado pero esta desactivado");
                    }
                    frmproveedores frmprov = new frmproveedores();
                    this.Close();
                    frmprov.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error de busqueda " + ex.Message);
            }
        }
        //Evento para volver a la pantalla Proveedor
        private void btncancelar_Click(object sender, EventArgs e)
        {
            frmproveedores frmprov = new frmproveedores();
            this.Close();
            frmprov.ShowDialog();
        }
        //Evento para salir de la pantalla
        private void btnsalir_Click(object sender, EventArgs e)
        {
            Close();
        }
        //Evento que valida si se activa o no el mensaje del errorprovider en el texbox de nombre
        private void txtnom_Leave(object sender, EventArgs e)
        {
            if (txtnom.Text == "")
            {
                error.SetError(txtnom, "Dato obligatorio");
            }
        }
        //Evento que valida si se activa o no el mensaje del errorprovider en el texbox de direccion
        private void txtdir_Leave(object sender, EventArgs e)
        {
            if (txtdir.Text == "")
            {
                error.SetError(txtdir, "Dato obligatorio");
            }
        }
        //Evento que valida si se activa o no el mensaje del errorprovider en el texbox de representante
        private void txtrep_Leave(object sender, EventArgs e)
        {
            if (txtrep.Text == "")
            {
                error.SetError(txtrep, "Dato obligatorio");
            }
        }
        //Evento que valida si se desactiva o no el mensaje del errorprovider en el texbox de nombre
        private void txtnom_TextChanged(object sender, EventArgs e)
        {
            if (txtnom.Text != "")
            {
                error.SetError(txtnom, "");
            }
        }
        //Evento que valida si se desactiva o no el mensaje del errorprovider en el texbox de direccion
        private void txtdir_TextChanged(object sender, EventArgs e)
        {
            if (txtdir.Text != "")
            {
                error.SetError(txtdir, "");
            }
        }
        //Evento que valida si se desactiva o no el mensaje del errorprovider en el texbox de representante
        private void txtrep_TextChanged(object sender, EventArgs e)
        {
            if (txtrep.Text != "")
            {
                error.SetError(txtrep, "");
            }
        }
        //Evento que valida el texbox de correo al momento de su salida y manda a validar
        private void txtcor_Leave(object sender, EventArgs e)
        {
            if (txtcor.Text != "")
            {
                validacor();
            }
        }
        //Metodo que valida el correo
        public void validacor()
        {
            string res = "";
            if (txtcor.Text != "")
            {
                if (txtcor.TextLength > 0 && txtcor.Text.Contains('@'))
                {
                    res = util.validacorreo(txtcor.Text);
                    if (res == "Invalido")
                    {
                        MessageBox.Show("Mal ingresado el correo");
                        txtcor.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Mal ingresado el correo");
                    txtcor.Focus();
                }
            }
        }

        private void txtruc_KeyPress(object sender, KeyPressEventArgs e)
        {
            char letra = e.KeyChar;
            if (!char.IsDigit(letra) && letra != 8 && letra != 13 && letra != 32)
                e.Handled = true;
        }

        private void validaletras(object sender, KeyPressEventArgs e)
        {
            char letra = e.KeyChar;
            if (!char.IsLetter(letra) && letra != 32 && letra != 8 && letra != 13)
                e.Handled = true;
        }

        private void validaletrasynumeros(object sender, KeyPressEventArgs e)
        {
            char letra = e.KeyChar;
            if (!char.IsLetterOrDigit(letra) && letra != 8 && letra != 13 && letra != 32 && letra != 64)
                e.Handled = true;
        }
    }
}
