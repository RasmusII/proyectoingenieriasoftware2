﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Farmacia.Controlador;

namespace Farmacia.Vista
{
    public partial class frmlote : Form
    {
        public frmlote()
        {
            InitializeComponent();
        }

        int fila = -1;
        string estado = "";

        private void frmlote_Load(object sender, EventArgs e)
        {
            dtpex.MaxDate = DateTime.Today;
            dtpca.MinDate = DateTime.Today.AddMonths(1);
            llenacbo(cbomed);
            cbomed.Text = "Seleccione...";
            llenainfo();
        }

        private void llenacbo(ComboBox cbo)
        {
            try
            {
                medicamentodb objM = new medicamentodb();
                //objM.getmed().Listamed = objM.traecuentacbo();
                objM.getmed().Listamed = objM.traemed("A");
                if (objM.getmed().Listamed.Count == 0)
                {
                    MessageBox.Show("No existen registros de cuentas a pagar");
                    panel2.Enabled = false;
                }
                cbo.DisplayMember = "Nom_med";
                cbo.ValueMember = "Id_med";
                cbo.DataSource = objM.getmed().Listamed;
                cbomed.Text = "Seleccione...";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en la presentacion de datos " + ex.Message);
            }
        }

        public void llenainfo()
        {
            try
            {
                dgvinfo.Rows.Clear();
                lotedb objL = new lotedb();
                objL.getlote().Listalote = objL.traelotes();
                if (objL.getlote().Listalote.Count == 0)
                {
                    MessageBox.Show("No hay registros");
                    txtbuscar.Enabled = false;
                    btnmodificar.Enabled = false;
                }
                else
                {
                    fila = 0;
                    for (int i = 0; i < objL.getlote().Listalote.Count; i++)
                    {
                        int num;
                        string nombremed = "";
                        dgvinfo.Rows.Add(1);
                        dgvinfo.Rows[i].Cells[0].Value = objL.getlote().Listalote[i].Id_lote;
                        dgvinfo.Rows[i].Cells[1].Value = objL.getlote().Listalote[i].Fec_ela;
                        dgvinfo.Rows[i].Cells[2].Value = objL.getlote().Listalote[i].Fec_cad;
                        num = objL.getlote().Listalote[i].Id_medic;
                        nombremed = objL.traenommed(num);
                        dgvinfo.Rows[i].Cells[3].Value = nombremed;
                    }
                    txtbuscar.Enabled = true;
                    btnmodificar.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }
        }

        private void btnnuevo_Click(object sender, EventArgs e)
        {
            estado = "N";
            panel2.Enabled = false;
            panel1.Enabled = true;
        }

        private void btnguardar_Click(object sender, EventArgs e)
        {
            if (txtlote.Text != "" && cbomed.Text != "Seleccione..." && dtpex.Value != DateTime.Now && dtpca.Value != DateTime.Now)
            {
                if (estado == "N")
                {
                    adicciona();
                }
                if (estado == "E")
                {
                    editar();
                }
                txtlote.Text = null;
                dtpex.MaxDate = DateTime.Today;
                dtpca.MinDate = DateTime.Today.AddMonths(1);
                cbomed.Text = "Seleccione...";
                panel2.Enabled = true;
                panel1.Enabled = false;
                txtlote.Text = null;
                llenainfo();
                
            }
            else
            {
                MessageBox.Show("Faltan datos");
            }
        }

        private void adicciona()
        {
            try
            {
                lotedb objL = new lotedb();
                int resp;
                llenalote(objL);
                resp = objL.insertalote(objL.getlote());
                if (resp == 0)
                {
                    MessageBox.Show("No se han ingresado datos");
                }
                else
                {
                    MessageBox.Show("Lote ingresado");
                    estado = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error  " + ex.Message);
            }
        }

        private lotedb llenalote(lotedb lot)
        {
            lot.getlote().Id_lote = txtlote.Text;
            lot.getlote().Fec_ela = util.girafecha(dtpex.Value.ToShortDateString());
            lot.getlote().Fec_cad = util.girafecha(dtpca.Value.ToShortDateString());
            lot.getlote().Id_medic =Convert.ToInt16(cbomed.SelectedValue.ToString());
            return lot;
        }

        private void btnmodificar_Click(object sender, EventArgs e)
        {
            modificar();
        }

        private void modificar()
        {
            try
            {
                lotedb objL = new lotedb();
                medicamentodb objM = new medicamentodb();
                objL.setlote(objL.traelote(dgvinfo.Rows[fila].Cells[0].Value.ToString()));
                if (objL.getlote().Id_lote == "")
                {
                    MessageBox.Show("Registro no existe");
                }
                else
                {
                    txtlote.Text = objL.getlote().Id_lote;
                    dtpex.Value = DateTime.Parse(objL.getlote().Fec_ela);
                    dtpca.Value = DateTime.Parse(objL.getlote().Fec_cad);
                    int idc = Convert.ToInt32(objL.getlote().Id_medic);
                    objM.setmed(objM.traemedic(idc.ToString()));
                    cbomed.Text = objM.getmed().Nom_med;                    
                    btnguardar.Enabled = true;
                    txtlote.Enabled = false;
                    estado = "E";
                    panel1.Enabled = true;
                    panel2.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }
        }

        private void dgvinfo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            fila = dgvinfo.CurrentRow.Index;
        }

        private void editar()
        {
            try
            {
                lotedb objL = new lotedb();
                int resp;
                llenalote(objL);
                resp = objL.Actualizalote(objL.getlote());
                if (resp == 0)
                {
                    MessageBox.Show("No se modifico el lote");
                }
                else
                {
                    MessageBox.Show("Lote modificado");
                    estado = "";
                    llenainfo();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }
        }

        private void btnbuscar_Click(object sender, EventArgs e)
        {
            buscar(txtbuscar.Text, 0);
        }

        private void buscar(string txt, int a)
        {
            try
            {
                lotedb objL = new lotedb();
                medicamentodb objM = new medicamentodb();
                objL.getlote().Listalote = objL.buscalote(txt);
                if (objL.getlote().Listalote.Count > 0 && a == 1)
                {
                    if (MessageBox.Show("El lote " + objL.getlote().Listalote[0].Id_lote + " ya esta registrado\nDesea modificarlo", "Informacion", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        for (int i = 0; i < objL.getlote().Listalote.Count; i++)
                        {
                            txtlote.Text = objL.getlote().Listalote[0].Id_lote;
                            dtpex.Text = objL.getlote().Listalote[0].Fec_ela;
                            dtpca.Text = objL.getlote().Listalote[0].Fec_cad;
                            int idc = objL.getlote().Listalote[0].Id_medic;
                            objM.setmed(objM.traemedic(idc.ToString()));
                            cbomed.Text = objM.getmed().Nom_med;
                            btnguardar.Enabled = true;
                            estado = "E";
                            panel1.Enabled = true;
                            panel2.Enabled = false;
                        }
                    }
                }
                if (objL.getlote().Listalote.Count >= 0 && a == 0)
                {
                    dgvinfo.Rows.Clear();
                    fila = 0;
                    for (int i = 0; i < objL.getlote().Listalote.Count; i++)
                    {
                        int num;
                        string nombremed = "";
                        dgvinfo.Rows.Add(1);
                        dgvinfo.Rows[i].Cells[0].Value = objL.getlote().Listalote[i].Id_lote;
                        dgvinfo.Rows[i].Cells[1].Value = objL.getlote().Listalote[i].Fec_ela;
                        dgvinfo.Rows[i].Cells[2].Value = objL.getlote().Listalote[i].Fec_cad;
                        num = objL.getlote().Listalote[i].Id_medic;
                        nombremed = objL.traenommed(num);
                        dgvinfo.Rows[i].Cells[3].Value = nombremed;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error de busqueda " + ex.Message);
            }
        }

        private void txtbuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            char letra = e.KeyChar;
            if (!char.IsLetterOrDigit(letra) && letra != 13 && letra != 8)
                e.Handled = true;
        }

        private void txtbuscar_TextChanged(object sender, EventArgs e)
        {
            if (txtbuscar.Text != "")
            {
                btnbuscar.Enabled = true;
            }
            else
            {
                llenainfo();
                btnbuscar.Enabled = false;
            }
        }

        private void txtlote_Leave(object sender, EventArgs e)
        {
            buscar(txtlote.Text, 1);
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            util.mustrapantallas("L", 1);
            Close();
        }

        private void frmlote_FormClosed(object sender, FormClosedEventArgs e)
        {
            util.mustrapantallas("L", 1);
        }

        private void cbomed_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
    }
}
