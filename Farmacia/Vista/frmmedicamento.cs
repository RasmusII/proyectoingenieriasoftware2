﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Farmacia.Controlador;

namespace Farmacia.Vista
{
    public partial class frmmedicamento : Form
    {
        public frmmedicamento()
        {
            InitializeComponent();
        }

        int fila = -1;//Variable que guarda el numero de la celda seleccionada
        string estado = "";//Variable que guarda si la informacion va a ser nueva o modificada
        int idmed;//Variable que guarda el id_medicamento 
        //Evento para salir de la pantalla
        private void btnsalir_Click(object sender, EventArgs e)
        {
            util.mustrapantallas("M", 1);
            this.Close();
        }
        //Metodo para buscar el medicamento
        private void buscar(string txt, string est)
        {
            try
            {
                dgvlistadomed.Rows.Clear();
                medicamentodb objM = new medicamentodb();
                objM.getmed().Listamed = objM.buscamed(txt, est);
                if (est == "M")
                {
                    if (objM.getmed().Listamed.Count > 0 && objM.getmed().Listamed[0].Tipo_med==cbotipomed.Text)
                    {
                        if (objM.getmed().Listamed[0].Est_med == "A")
                        {
                            MessageBox.Show("El nombre del medicamento ya esta ingredo");
                        }
                        else
                        {
                            MessageBox.Show("El nombre del medicamento ya esta ingredo\nPara poder volverlo a vizualizar deberá activarlo");
                        }
                        txtnommed.Focus();
                    }
                }
                else
                {
                    if (objM.getmed().Listamed.Count > 0)
                    {
                        fila = 0;
                        for (int i = 0; i < objM.getmed().Listamed.Count; i++)
                        {
                            dgvlistadomed.Rows.Add(1);
                            dgvlistadomed.Rows[i].Cells[0].Value = objM.getmed().Listamed[i].Id_med;
                            dgvlistadomed.Rows[i].Cells[1].Value = objM.getmed().Listamed[i].Nom_med; ;
                            dgvlistadomed.Rows[i].Cells[2].Value = objM.getmed().Listamed[i].Stock; ;
                            dgvlistadomed.Rows[i].Cells[3].Value = objM.getmed().Listamed[i].Pre_ven;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Medicamento no registrado");
                        txtbuscar.Text = null;
                    }
                }                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error de busqueda " + ex.Message);
            }
        }
        //Evento que define que se hara u nnuevo registro
        private void btnnuevo_Click(object sender, EventArgs e)
        {
            estado = "N";
            panel2.Enabled = false;
            panel1.Enabled = true;
            txtnommed.Enabled = true;
            cbotipomed.Enabled = true;
            cbotipomed.Focus();
        }
        //Evento que manda la informacion a diferentes metodos, deprndiendo de lo que se desea (Modificar o nuevo)
        private void btnguardar_Click(object sender, EventArgs e)
        {
            if (cbotipomed.Text != "Seleccione...." && txtpreciov.Text != "" && txtnommed.Text != "")
            {
                if (estado == "N")
                {
                    adicciona();
                }
                if (estado == "E")
                {
                    editar();
                }
                panel2.Enabled = true;
                panel1.Enabled = false;
                llenamedicamentos("A");
                limpiar();
            }
            else
            {
                MessageBox.Show("No ha ingresado todos los datos");
                valida();
            }
        }
        //Metodo que envia un error si no se han escrito en los campos requeridos
        private void valida()
        {
            if (cbotipomed.Text == "Seleccione....")
            {
                error.SetError(cbotipomed, "Este campo es obligatorio");
            }
            if (txtnommed.Text == "")
            {
                error.SetError(txtnommed, "Este campo es obligatorio");
            }
            if (txtpreciov.Text == "")
            {
                error.SetError(txtpreciov, "Este campo es obligatorio");
            }
        }
        //Metodo que limpia la informacion ingresada
        private void limpiar()
        {
            cbotipomed.Text = "Seleccione....";
            txtnommed.Text = null;
            txtprecioc.Text = null;
            txtpreciov.Text = null;
            txtstock.Text = null;
        }
        //Metodo que manda a guardar la informacion ingrasada
        private void adicciona()
        {
            try
            {
                medicamentodb objM = new medicamentodb();
                int resp;
                llenamedicamento(objM);
                resp = objM.insertamed(objM.getmed());
                if (resp == 0)
                {
                    MessageBox.Show("No se han ingresado datos");
                }
                else
                {
                    MessageBox.Show("Medicamento ingresado");
                    txtbuscar.Enabled = true;
                    btnmodificar.Enabled = true;
                    btndes.Enabled = true;
                    estado = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error  " + ex.Message);
            }
        }
        //Metodo para llenar la datagridview
        public void llenamedicamentos(string est)
        {
            try
            {
                dgvlistadomed.Rows.Clear();
                medicamentodb objM = new medicamentodb();
                objM.getmed().Listamed = objM.traemed(est);
                if (objM.getmed().Listamed.Count == 0)
                {
                    MessageBox.Show("No hay registros");
                    txtbuscar.Enabled = false;
                    btnmodificar.Enabled = false;
                    btndes.Enabled = false;
                }
                else
                {
                    fila = 0;
                    for (int i = 0; i < objM.getmed().Listamed.Count; i++)
                    {
                        dgvlistadomed.Rows.Add(1);
                        dgvlistadomed.Rows[i].Cells[0].Value = objM.getmed().Listamed[i].Id_med;
                        dgvlistadomed.Rows[i].Cells[1].Value = objM.getmed().Listamed[i].Nom_med;
                        dgvlistadomed.Rows[i].Cells[2].Value = objM.getmed().Listamed[i].Stock; ;
                        dgvlistadomed.Rows[i].Cells[3].Value = objM.getmed().Listamed[i].Pre_ven;
                    }
                    txtbuscar.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }
        }
        //Metodo que llenala clase Medicamento con toda la informacion obtenida
        private medicamentodb llenamedicamento(medicamentodb med)
        {
            int dis=txtpreciov.TextLength;
            med.getmed().Id_med = idmed;
            med.getmed().Tipo_med = cbotipomed.Text;
            med.getmed().Nom_med = txtnommed.Text;
            med.getmed().Pre_ven = double.Parse(txtpreciov.Text);
            if (txtprecioc.Text !="")
            {
                med.getmed().Pre_com = txtprecioc.Text;
            }            
            med.getmed().Est_med = "A";
            return med;
        }
        //Evento que valida que votones se debe desabilitar y llena la datagridview
        private void frmmedicamento_Load(object sender, EventArgs e)
        {
            if (dgvlistadomed.Rows.Count > 0)
            {
                btndes.Enabled = true;
                btnmodificar.Enabled=true;
            }
            llenamedicamentos("A");
        }
        //Evento que manda a pedir informacion a un metodo para luego mostrarla y modificarla
        private void btnmodificar_Click(object sender, EventArgs e)
        {
            modificar();
        }
        //Metodo que trae la informacion requerida para realizar las modificaciones correspondientes
        private void modificar()
        {
            try
            {
                medicamentodb objM = new medicamentodb();
                objM.setmed(objM.traemedic(dgvlistadomed.Rows[fila].Cells[0].Value.ToString()));
                if (objM.getmed().Id_med.ToString() == "")
                {
                    MessageBox.Show("Registro no existe");
                }
                else
                {
                    idmed = objM.getmed().Id_med;
                    cbotipomed.Text = objM.getmed().Tipo_med;
                    txtnommed.Text = objM.getmed().Nom_med;
                    txtpreciov.Text = objM.getmed().Pre_ven.ToString();
                    txtprecioc.Text = objM.getmed().Pre_com.ToString();
                    txtstock.Text = objM.getmed().Stock.ToString();
                    estado = "E";
                    panel1.Enabled = true;
                    panel2.Enabled = false;
                    cbotipomed.Enabled = false;
                    txtnommed.Enabled = false;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }
        }
        //Evento que guarda el numero de la fila que se elije
        private void dgvlistadomed_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            fila = dgvlistadomed.CurrentRow.Index;
        }
        //Metodo que actualiza la informacion
        private void editar()
        {
            try
            {
                medicamentodb objM = new medicamentodb();
                int resp;
                llenamedicamento(objM);
                resp = objM.Actualizamed(objM.getmed());
                if (resp == 0)
                {
                    MessageBox.Show("No se modifico medicamento");
                }
                else
                {
                    MessageBox.Show("Medicamento modificado");
                    estado = "";
                    llenamedicamentos("A");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }
        }
        //Evento que manda a limpiar los texbox e inhabilitar el panel 1 
        private void btncancelar_Click(object sender, EventArgs e)
        {
            limpiar();
            if (cbotipomed.Text == "Seleccione....")
            {
                error.SetError(cbotipomed, "");
            }
            if (txtnommed.Text == "")
            {
                error.SetError(txtnommed, "");
            }
            if (txtpreciov.Text == "")
            {
                error.SetError(txtpreciov, "");
            }
            panel2.Enabled = true;
            panel1.Enabled = false;
        }
        //Evento que manda a desactivar un medicamento
        private void btndes_Click(object sender, EventArgs e)
        {
            desactiva();
        }
        //Metodo que desactiva un medicamento
        private void desactiva()
        {
            try
            {
                medicamentodb objM = new medicamentodb();
                int resp;
                string mensaje = "";
                string id = dgvlistadomed.Rows[fila].Cells[0].Value.ToString();
                if (MessageBox.Show("Desea " + btndes.Text + " a " + dgvlistadomed.Rows[fila].Cells[1].Value.ToString(), "Empresa", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    if (btndes.Text.Equals("Desactivar"))
                    {
                        resp = objM.desactivamed(id, "P");
                        mensaje = "Desactivado";
                    }
                    else
                    {
                        resp = objM.desactivamed(id, "A");
                        mensaje = "Activado";
                        chkdes.Checked = false;
                    }
                    if (resp > 0)
                    {
                        MessageBox.Show("Medicamento " + mensaje);
                        llenamedicamentos("A");
                    }
                    else
                    {
                        MessageBox.Show("No se elimino empleado ");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex.Message);
            }
        }
        //Evento que verifica si se deben mostrar medicamentos activos o desactivados
        private void chkdes_CheckedChanged(object sender, EventArgs e)
        {
            if (chkdes.Checked == true)
            {
                btnmodificar.Enabled = false;
                btnnuevo.Enabled = false;
                llenamedicamentos("P");
                btndes.Text = "Activar";
                if (dgvlistadomed.Rows.Count > 0)
                {
                    btndes.Enabled = true;
                }
                else
                {
                    btndes.Enabled = false;
                }
            }
            else
            {
                llenamedicamentos("A");
                btnnuevo.Enabled = true;
                btndes.Text = "Desactivar";
                if (dgvlistadomed.Rows.Count > 0)
                {
                    btnmodificar.Enabled = true;
                    btndes.Enabled = true;
                }
                else
                {
                    btnmodificar.Enabled = false;
                    btndes.Enabled = false;
                }
            }
        }
        //Evento que verifica si tiene que mandar a buscar un medicamento activo o desactivado
        private void txtbuscar_TextChanged(object sender, EventArgs e)
        {
            if (txtbuscar.Text != "")
            {
                if (chkdes.Checked == true)
                {
                    buscar(txtbuscar.Text, "P"); 
                }
                else
                {
                    buscar(txtbuscar.Text, "A");
                }                
            }
            else
            {
                if (chkdes.Checked == true)
                {
                    llenamedicamentos("P");                    
                }
                else
                {
                    llenamedicamentos("A");
                }
            }
        }
        //Evento que valida la entrada de numeros
        private void txtpreciov_KeyPress(object sender, KeyPressEventArgs e)
        {
            char letra = e.KeyChar;
            if (!char.IsDigit(letra) && letra != 13 && letra != 8 && letra != 46)
                e.Handled = true;
            if (letra == 46 && txtpreciov.Text.Contains('.'))
                e.Handled = true;
        }
        //Evento que valida si se pone el errorprovider o no
        private void cbotipomed_Leave(object sender, EventArgs e)
        {
            if (cbotipomed.Text == "Seleccione....")
            {
                error.SetError(cbotipomed, "Este campo es obligatorio");
            }
            else
            {
                error.SetError(cbotipomed, "");
            }
        }
        //Evento que valida si se pone el errorprovider o no
        private void txtnommed_Leave(object sender, EventArgs e)
        {
            if (txtnommed.Text == "")
            {
                error.SetError(txtnommed, "Este campo es obligatorio");
            }
            else
            {
                buscar(txtnommed.Text, "M");
            }
        }
        //Evento que valida si se pone el errorprovider o no
        private void txtpreciov_Leave(object sender, EventArgs e)
        {
            if (txtpreciov.Text == "")
            {
                error.SetError(txtpreciov, "Este campo es obligatorio");
            }
        }
        //Evento que valida si se pone el errorprovider o no
        private void txtnommed_TextChanged(object sender, EventArgs e)
        {
            if (txtnommed.Text != "")
            {
                error.SetError(txtnommed, "");
            }
        }
        //Evento que valida si se pone el errorprovider o no
        private void txtpreciov_TextChanged(object sender, EventArgs e)
        {
            if (txtpreciov.Text != "")
            {
                error.SetError(txtpreciov, "");
            }
        }
        //Evento que no permite escribir en el combobox
        private void cbotipomed_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void frmmedicamento_FormClosed(object sender, FormClosedEventArgs e)
        {
            util.mustrapantallas("M",1);
        }

        private void txtbuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            char letra = e.KeyChar;
            if (!char.IsLetter(letra) && letra != 32 && letra != 8 && letra != 13)
                e.Handled = true;
        }
    }
}
