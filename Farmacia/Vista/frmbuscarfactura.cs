﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Farmacia.Controlador;

namespace Farmacia.Vista
{
    public partial class frmbuscarfactura : Form
    {
        public frmbuscarfactura()
        {
            InitializeComponent();
        }

        private void frmbuscarfactura_Load(object sender, EventArgs e)
        {
            llenainfo();
        }

        private void llenainfo()
        {
            try
            {
                dgvinfo.Rows.Clear();
                facturadb objF = new facturadb();
                objF.getfactura().Listafactura = objF.traefactura();
                if (objF.getfactura().Listafactura.Count == 0)
                {
                    MessageBox.Show("No hay registros");
                }
                else
                {
                    for (int i = 0; i < objF.getfactura().Listafactura.Count; i++)
                    {
                        dgvinfo.Rows.Add(1);
                        dgvinfo.Rows[i].Cells[0].Value = objF.getfactura().Listafactura[i].Num_fact;                        
                        clientedb objE = new clientedb();
                        objE.getcliente().Listacliente = objE.buscacliente(objF.getfactura().Listafactura[i].Id_cli.ToString(),"O");
                        dgvinfo.Rows[i].Cells[1].Value = objE.getcliente().Listacliente[0].Nombre;
                        dgvinfo.Rows[i].Cells[2].Value = objF.getfactura().Listafactura[i].Fec_emi;
                        dgvinfo.Rows[i].Cells[3].Value = objF.getfactura().Listafactura[i].Subtotal;
                        dgvinfo.Rows[i].Cells[4].Value = objF.getfactura().Listafactura[i].Iva;
                        dgvinfo.Rows[i].Cells[5].Value = objF.getfactura().Listafactura[i].Total;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }
        }

        private void btnbuscar_Click(object sender, EventArgs e)
        {
            if (txtbuscar.Text != "")
            {
                int num = int.Parse(txtbuscar.Text);
                buscar(num);
            }
            else
            {
                llenainfo();
            }
        }

        private void buscar(int n)
        {
            try
            {
                dgvinfo.Rows.Clear();
                facturadb objF = new facturadb();
                objF.getfactura().Listafactura = objF.buscafactura(n);
                if (objF.getfactura().Listafactura.Count > 0)
                {
                    for (int i = 0; i < objF.getfactura().Listafactura.Count; i++)
                    {
                        dgvinfo.Rows.Add(1);
                        dgvinfo.Rows[i].Cells[0].Value = objF.getfactura().Listafactura[i].Num_fact;
                        clientedb objE = new clientedb();
                        objE.getcliente().Listacliente = objE.buscacliente(objF.getfactura().Listafactura[i].Id_cli.ToString(), "O");
                        dgvinfo.Rows[i].Cells[1].Value = objE.getcliente().Listacliente[0].Nombre;
                        dgvinfo.Rows[i].Cells[2].Value = objF.getfactura().Listafactura[i].Fec_emi;
                        dgvinfo.Rows[i].Cells[3].Value = objF.getfactura().Listafactura[i].Subtotal;
                        dgvinfo.Rows[i].Cells[4].Value = objF.getfactura().Listafactura[i].Iva;
                        dgvinfo.Rows[i].Cells[5].Value = objF.getfactura().Listafactura[i].Total;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error de busqueda " + ex.Message);
            }
        }

        private void txtbuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            char letra = e.KeyChar;
            if (!char.IsDigit(letra) && letra != 8 && letra != 13)
                e.Handled = true;
        }

        private void dgvinfo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int fila = dgvinfo.CurrentRow.Index;
            if (this.dgvinfo.Columns[e.ColumnIndex].Name == "Column4")
            {
                string num = dgvinfo.Rows[fila].Cells[0].Value.ToString();
                frmreportes objR = new frmreportes(num);
                objR.Show();
            }
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            util.mustrapantallas("BF", 1);
        }

        private void frmbuscarfactura_FormClosed(object sender, FormClosedEventArgs e)
        {
            util.mustrapantallas("BF", 1);
        }


    }
}
