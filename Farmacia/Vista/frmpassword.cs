﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Farmacia.Controlador;

namespace Farmacia.Vista
{
    public partial class frmpassword : Form
    {
        public frmpassword(int num)
        {
            InitializeComponent();
            this.num = num;
        }
        int num;//Variable que guarda un numerto enviado de otras pantallas
        string res = "";//Variable que guarda la informacion de la cedula si es valida o invalida
        //Evento para cancelar y cerrar la pantalla
        private void btncancelar_Click(object sender, EventArgs e)
        {
            if (num == 0)
            {
                Application.Exit();
            }
            else
            {
                this.Hide();
            } 
        }
        //Evento que valida la salida de la pantalla
        private void frmpassword_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (num == 0)
            {
                Application.Exit();
            }
            else
            {
                this.Hide();
            }            
        }
        //Evento que manda a traer la cedula dependiendo si es nuevo o va a modificar
        private void frmpassword_Load(object sender, EventArgs e)
        {
            if (num == 0)
            {
                txtced.Focus();
            }
            else
            {
                traecontraseña();
                txtcontras.Focus();
            }
        }
        //Metodo que trae la contraseña de la entidad Gerente
        private void traecontraseña()
        {
            try
            {
                iniciarsesiondb objI = new iniciarsesiondb();
                objI.setlogin(objI.traelogin("A001"));
                if (objI.getlogin().Est_ad == "")
                {
                    MessageBox.Show("No se encontraron datos");
                }
                else
                {
                    txtced.Text = objI.getlogin().Ced_ad.ToString();
                    txtnom.Text = objI.getlogin().Nom_ad;
                    txtape.Text = objI.getlogin().Ape_ad;
                    txttel.Text = objI.getlogin().Tel_ad.ToString();
                    txtusu.Text = objI.getlogin().Ced_ad.ToString();
                    txtced.Enabled = false;
                    txtnom.Enabled = false;
                    txtape.Enabled = false;
                }
                clientedb objC = new clientedb();
                objC.setcliente(objC.traecliente(txtced.Text));
                txtcor.Text = objC.getcliente().Cor_cli;
                txtdir.Text = objC.getcliente().Dir_cli;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en los datos, " + ex.Message);
            }
        }        
        //Evento que guarda la informacion proporcionada
        private void btnguardar_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void guardar()
        {
            if (txtnom.Text != "" && txtced.Text != "" && txtcontras.Text != "" && txtconcontras.Text != "" && txtape.Text != "" && txtcor.Text != "" && txtdir.Text != "")
            {
                if (txtcontras.Text == txtconcontras.Text)
                {
                    try
                    {
                        int resp = 0;
                        if (num == 0)
                        {
                            clientedb objC = new clientedb();
                            llenacliente(objC);
                            resp = objC.insertacliente(objC.getcliente());
                        }
                        resp = 0;
                        iniciarsesiondb objI = new iniciarsesiondb();
                        llenaadmin(objI);
                        resp = objI.insertaadmin(objI.getlogin(), num);
                        if (resp == 0)
                        {
                            MessageBox.Show("No se han ingresado datos");
                        }
                        else
                        {
                            if (num == 0)
                            {
                                MessageBox.Show("Gerente ingresado con exito");
                                Form1 objF = new Form1(0);
                                this.Hide();
                                objF.ShowDialog();
                            }
                            else
                            {
                                resp = 0;
                                clientedb objC = new clientedb();
                                llenacliente(objC);
                                resp = objC.Actualizacliente(objC.getcliente());
                                MessageBox.Show("Contraseña cambiada con exito");
                                this.Hide();
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error  " + ex.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Verifique las contraseñas");
                }
            }
            else
            {
                MessageBox.Show("Faltan datos por ingresar");
                validarcampo();
            }
        }
        //Evento que manda error a los campos que no se han ingresado informacion
        private void validarcampo()
        {
            if (txtnom.Text == "")
            {
                error.SetError(txtnom, "Ingrese el nombre");
            }
            if (txtced.Text == "")
            {
                error.SetError(txtced, "Ingrese la cedula");
            }
            if (txtape.Text == "")
            {
                error.SetError(txtape, "Ingrese el apellido");
            }
            if (txtcontras.Text == "")
            {
                error.SetError(txtcontras, "Ingrese una contraseña");
            } 
            if (txtconcontras.Text == "")
            {
                error.SetError(txtconcontras, "Ingrese nuevamente su contraseña");
            }
            if (txtcor.Text == "")
            {
                error.SetError(txtcor, "Ingrese nuevamente su correo");
            }
            if (txtdir.Text == "")
            {
                error.SetError(txtdir, "Ingrese nuevamente su direccion");
            }
        }
        //Metodo que llena los datos a la clase Iniciar sesion
        private iniciarsesiondb llenaadmin(iniciarsesiondb iniciar)
        {
            iniciar.getlogin().Id_ad = "A001";
            iniciar.getlogin().Ced_ad = int.Parse(txtced.Text);
            iniciar.getlogin().Nom_ad = txtnom.Text;
            iniciar.getlogin().Ape_ad = txtape.Text;
            iniciar.getlogin().Cor_ad = label.Text;
            iniciar.getlogin().Dir_ad = txtdir.Text;
            if (txttel.Text == "")
            {
                iniciar.getlogin().Tel_ad = "0";
            }
             else
            {
                iniciar.getlogin().Tel_ad = txttel.Text;
            }
            iniciar.getlogin().Log_ad = txtcontras.Text;
            iniciar.getlogin().Est_ad = "A";
            return iniciar;
        }
        //Metodo que llena los datos a la clase Cliente
        private clientedb llenacliente(clientedb iniciar)
        {
            iniciar.getcliente().Ced_cli = int.Parse(txtced.Text);
            iniciar.getcliente().Nom_cli = txtnom.Text;
            iniciar.getcliente().Ape_cli = txtape.Text;
            iniciar.getcliente().Cor_cli = txtcor.Text;
            iniciar.getcliente().Dir_cli = txtdir.Text;
            if (txttel.Text == "")
            {
                iniciar.getcliente().Tel_cli = "0";
            }
            else
            {
                iniciar.getcliente().Tel_cli = txttel.Text;
            }
            iniciar.getcliente().Est_cli = "A";
            return iniciar;
        }
        //Evento que valida la cedula al salir del texbox
        private void txtced_Leave(object sender, EventArgs e)
        {
            validaced();
        }

        private void validaced()
        {

            if (txtced.Text == "")
            {
                error.SetError(txtced, "Debe ingresar la cedula obligatoriamente");
            }
            res = util.validacedula(txtced.Text);
            if (res == "Invalido")
            {
                MessageBox.Show("El numero de cedula no es correcto");
                txtced.Focus();
                txtced.SelectAll();
            }
            else
            {
                txtusu.Text = txtced.Text;
            }   
        }
        //Evento que valida la entrada de numeros
        private void validanumeros(object sender, KeyPressEventArgs e)
        {
            char letra = e.KeyChar;
            if (!char.IsDigit(letra) && letra != 8 && letra != 13 && letra != 32)
                e.Handled = true;
        }
        //Evento que valida la entrada de letras
        private void validaletras(object sender, KeyPressEventArgs e)
        {
            char letra = e.KeyChar;
            if (!char.IsLetter(letra) && letra != 32 && letra != 8 && letra != 13)
                e.Handled = true;
        }
        //Evento que manda el mensaje del errorprovider
        private void txtced_TextChanged(object sender, EventArgs e)
        {
            if (txtced.Text != "")
            {
                error.SetError(txtced, "");
            }
        }
        //Evento que manda el mensaje del errorprovider
        private void txtnom_Leave(object sender, EventArgs e)
        {
            if (txtnom.Text == "")
            {
                error.SetError(txtnom, "Debe ingresar el nombre obligatoriamente");
            }
        }
        //Evento que manda el mensaje del errorprovider
        private void txtnom_TextChanged(object sender, EventArgs e)
        {
            if (txtnom.Text != "")
            {
                error.SetError(txtnom, "");
            }
        }
        //Evento que manda el mensaje del errorprovider
        private void txtape_Leave(object sender, EventArgs e)
        {
            if (txtape.Text == "")
            {
                error.SetError(txtape, "Debe ingresar el apellido obligatoriamente");
            }
        }
        //Evento que manda el mensaje del errorprovider
        private void txtape_TextChanged(object sender, EventArgs e)
        {
            if (txtape.Text != "")
            {
                error.SetError(txtape, "");
            }
        }
        //Evento que manda el mensaje del errorprovider
        private void txtcontras_Leave(object sender, EventArgs e)
        {
            if (txtcontras.Text == "")
            {
                error.SetError(txtcontras, "Debe ingresar una contraseña obligatoriamente");
            }
        }
        //Evento que manda el mensaje del errorprovider
        private void txtcontras_TextChanged(object sender, EventArgs e)
        {
            if (txtcontras.Text != "")
            {
                error.SetError(txtcontras, "");
            }
        }
        //Evento que manda el mensaje del errorprovider
        private void txtconcontras_Leave(object sender, EventArgs e)
        {
            if (txtconcontras.Text == "")
            {
                error.SetError(txtconcontras, "Debe ingresar la contraseña para verificacion");
            }
        }
        //Evento que manda el mensaje del errorprovider
        private void txtconcontras_TextChanged(object sender, EventArgs e)
        {
            if (txtconcontras.Text != "")
            {
                error.SetError(txtconcontras, "");
            }
        }
        //Evento que manda el mensaje del errorprovider
        private void txtcor_TextChanged(object sender, EventArgs e)
        {

            if (txtcor.Text != "")
            {
                error.SetError(txtcor, "");
            }           
           
        }
        //Evento que manda el mensaje del errorprovider
        private void txtcor_Leave(object sender, EventArgs e)
        {
            if (txtcor.Text == "")
            {
                error.SetError(txtcor, "Es obligatorio poner su correo");
            }
            else
            {
                validacor();
            }
        }
        //Evento que manda el mensaje del errorprovider
        private void txtdir_Leave(object sender, EventArgs e)
        {
            if (txtdir.Text == "")
            {
                error.SetError(txtdir, "Es obligatorio poner su direccion");
            }
        }
        //Evento que manda el mensaje del errorprovider
        private void txtdir_TextChanged(object sender, EventArgs e)
        {
            if (txtdir.Text != "")
            {
                error.SetError(txtdir, "");
            }
        }
        //Metodo para enviar a validar el correo
        public void validacor()
        {
            string res = "";
            if (txtcor.Text != "")
            {
                if (txtcor.TextLength > 0 && txtcor.Text.Contains('@'))
                {
                    res = util.validacorreo(txtcor.Text);
                    if (res == "Invalido")
                    {
                        MessageBox.Show("Mal ingresado el correo");
                        txtcor.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Mal ingresado el correo");
                    txtcor.Focus();
                }
            }
        }

    }
}
