﻿namespace Farmacia.Vista
{
    partial class frmcompra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnguardar = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dtpfcad = new System.Windows.Forms.DateTimePicker();
            this.dtpfe = new System.Windows.Forms.DateTimePicker();
            this.lblfcad = new System.Windows.Forms.Label();
            this.lblfelab = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.dgvinfo2 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtnumfactura = new System.Windows.Forms.TextBox();
            this.cbopro = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.numcant = new System.Windows.Forms.NumericUpDown();
            this.cbomed = new System.Windows.Forms.ComboBox();
            this.cbolote = new System.Windows.Forms.ComboBox();
            this.txtpre = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dtpfact = new System.Windows.Forms.DateTimePicker();
            this.txtcodcom = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnsalir = new System.Windows.Forms.Button();
            this.dgvinfo = new System.Windows.Forms.DataGridView();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.preciototal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Eliminar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnguarda = new System.Windows.Forms.Button();
            this.txttotal = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.errorP = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvinfo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numcant)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvinfo)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorP)).BeginInit();
            this.SuspendLayout();
            // 
            // btnguardar
            // 
            this.btnguardar.Location = new System.Drawing.Point(545, 123);
            this.btnguardar.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.Size = new System.Drawing.Size(61, 25);
            this.btnguardar.TabIndex = 6;
            this.btnguardar.Text = "Agregar";
            this.btnguardar.UseVisualStyleBackColor = true;
            this.btnguardar.Click += new System.EventHandler(this.btnguardar_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dtpfcad);
            this.panel2.Controls.Add(this.dtpfe);
            this.panel2.Controls.Add(this.lblfcad);
            this.panel2.Controls.Add(this.lblfelab);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.dgvinfo2);
            this.panel2.Controls.Add(this.txtnumfactura);
            this.panel2.Controls.Add(this.cbopro);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.numcant);
            this.panel2.Controls.Add(this.cbomed);
            this.panel2.Controls.Add(this.cbolote);
            this.panel2.Controls.Add(this.btnguardar);
            this.panel2.Controls.Add(this.txtpre);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.shapeContainer1);
            this.panel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.panel2.Location = new System.Drawing.Point(12, 36);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(631, 170);
            this.panel2.TabIndex = 2;
            // 
            // dtpfcad
            // 
            this.dtpfcad.Enabled = false;
            this.dtpfcad.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpfcad.Location = new System.Drawing.Point(235, 141);
            this.dtpfcad.Name = "dtpfcad";
            this.dtpfcad.Size = new System.Drawing.Size(78, 20);
            this.dtpfcad.TabIndex = 46;
            this.dtpfcad.Visible = false;
            // 
            // dtpfe
            // 
            this.dtpfe.Enabled = false;
            this.dtpfe.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpfe.Location = new System.Drawing.Point(85, 141);
            this.dtpfe.Name = "dtpfe";
            this.dtpfe.Size = new System.Drawing.Size(80, 20);
            this.dtpfe.TabIndex = 45;
            this.dtpfe.Visible = false;
            // 
            // lblfcad
            // 
            this.lblfcad.AutoSize = true;
            this.lblfcad.Location = new System.Drawing.Point(171, 146);
            this.lblfcad.Name = "lblfcad";
            this.lblfcad.Size = new System.Drawing.Size(58, 13);
            this.lblfcad.TabIndex = 44;
            this.lblfcad.Text = "Fecha cad";
            this.lblfcad.Visible = false;
            // 
            // lblfelab
            // 
            this.lblfelab.AutoSize = true;
            this.lblfelab.Location = new System.Drawing.Point(19, 146);
            this.lblfelab.Name = "lblfelab";
            this.lblfelab.Size = new System.Drawing.Size(60, 13);
            this.lblfelab.TabIndex = 43;
            this.lblfelab.Text = "Fecha elab";
            this.lblfelab.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(391, 122);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(14, 16);
            this.label14.TabIndex = 42;
            this.label14.Text = "*";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(391, 92);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(14, 16);
            this.label15.TabIndex = 41;
            this.label15.Text = "*";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(70, 6);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(14, 16);
            this.label10.TabIndex = 38;
            this.label10.Text = "*";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(70, 122);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(14, 16);
            this.label11.TabIndex = 37;
            this.label11.Text = "*";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(70, 95);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(14, 16);
            this.label12.TabIndex = 36;
            this.label12.Text = "*";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(399, 7);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(14, 16);
            this.label13.TabIndex = 35;
            this.label13.Text = "*";
            // 
            // dgvinfo2
            // 
            this.dgvinfo2.AllowUserToAddRows = false;
            this.dgvinfo2.AllowUserToDeleteRows = false;
            this.dgvinfo2.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvinfo2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvinfo2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvinfo2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.dgvinfo2.Enabled = false;
            this.dgvinfo2.Location = new System.Drawing.Point(9, 30);
            this.dgvinfo2.Name = "dgvinfo2";
            this.dgvinfo2.ReadOnly = true;
            this.dgvinfo2.RowHeadersVisible = false;
            this.dgvinfo2.Size = new System.Drawing.Size(355, 45);
            this.dgvinfo2.TabIndex = 34;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Empresa";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Telefono";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 80;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Direccion";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 170;
            // 
            // txtnumfactura
            // 
            this.txtnumfactura.Location = new System.Drawing.Point(414, 3);
            this.txtnumfactura.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtnumfactura.MaxLength = 10;
            this.txtnumfactura.Name = "txtnumfactura";
            this.txtnumfactura.Size = new System.Drawing.Size(160, 20);
            this.txtnumfactura.TabIndex = 1;
            this.txtnumfactura.TextChanged += new System.EventHandler(this.txtnumfactura_TextChanged);
            this.txtnumfactura.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtnumfactura_KeyPress);
            this.txtnumfactura.Leave += new System.EventHandler(this.txtnumfactura_Leave);
            // 
            // cbopro
            // 
            this.cbopro.FormattingEnabled = true;
            this.cbopro.Location = new System.Drawing.Point(85, 2);
            this.cbopro.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbopro.Name = "cbopro";
            this.cbopro.Size = new System.Drawing.Size(213, 21);
            this.cbopro.TabIndex = 0;
            this.cbopro.SelectedIndexChanged += new System.EventHandler(this.cbopro_SelectedIndexChanged);
            this.cbopro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbopro_KeyPress);
            this.cbopro.Leave += new System.EventHandler(this.cbopro_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(305, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Numero de Factura";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 6);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Proveedor";
            // 
            // numcant
            // 
            this.numcant.Location = new System.Drawing.Point(406, 88);
            this.numcant.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numcant.Name = "numcant";
            this.numcant.Size = new System.Drawing.Size(84, 20);
            this.numcant.TabIndex = 4;
            this.numcant.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // cbomed
            // 
            this.cbomed.FormattingEnabled = true;
            this.cbomed.Location = new System.Drawing.Point(85, 87);
            this.cbomed.Name = "cbomed";
            this.cbomed.Size = new System.Drawing.Size(238, 21);
            this.cbomed.TabIndex = 2;
            this.cbomed.SelectedIndexChanged += new System.EventHandler(this.cbomed_SelectedIndexChanged);
            this.cbomed.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbomed_KeyPress);
            this.cbomed.Leave += new System.EventHandler(this.cbomed_Leave);
            // 
            // cbolote
            // 
            this.cbolote.FormattingEnabled = true;
            this.cbolote.Location = new System.Drawing.Point(85, 114);
            this.cbolote.Name = "cbolote";
            this.cbolote.Size = new System.Drawing.Size(161, 21);
            this.cbolote.TabIndex = 3;
            this.cbolote.SelectedIndexChanged += new System.EventHandler(this.cbolote_SelectedIndexChanged);
            this.cbolote.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbolote_KeyPress);
            // 
            // txtpre
            // 
            this.txtpre.Location = new System.Drawing.Point(406, 118);
            this.txtpre.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtpre.MaxLength = 10;
            this.txtpre.Name = "txtpre";
            this.txtpre.Size = new System.Drawing.Size(84, 20);
            this.txtpre.TabIndex = 5;
            this.txtpre.TextChanged += new System.EventHandler(this.txtpre_TextChanged);
            this.txtpre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtpre_KeyPress);
            this.txtpre.Leave += new System.EventHandler(this.txtpre_Leave);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label9.Location = new System.Drawing.Point(358, 121);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Precio";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label8.Location = new System.Drawing.Point(346, 90);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Cantidad";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label7.Location = new System.Drawing.Point(4, 92);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Medicamento";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label6.Location = new System.Drawing.Point(47, 119);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Lote";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(631, 170);
            this.shapeContainer1.TabIndex = 0;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.Enabled = false;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 2;
            this.lineShape1.X2 = 629;
            this.lineShape1.Y1 = 81;
            this.lineShape1.Y2 = 81;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.dtpfact);
            this.panel1.Controls.Add(this.txtcodcom);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Enabled = false;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(669, 30);
            this.panel1.TabIndex = 24;
            // 
            // dtpfact
            // 
            this.dtpfact.Enabled = false;
            this.dtpfact.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpfact.Location = new System.Drawing.Point(426, 4);
            this.dtpfact.Name = "dtpfact";
            this.dtpfact.Size = new System.Drawing.Size(100, 20);
            this.dtpfact.TabIndex = 8;
            // 
            // txtcodcom
            // 
            this.txtcodcom.Enabled = false;
            this.txtcodcom.Location = new System.Drawing.Point(109, 4);
            this.txtcodcom.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtcodcom.Name = "txtcodcom";
            this.txtcodcom.Size = new System.Drawing.Size(157, 20);
            this.txtcodcom.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(348, 10);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Fecha Actual";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 7);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Codigo de Compra";
            // 
            // btnsalir
            // 
            this.btnsalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsalir.Location = new System.Drawing.Point(570, 431);
            this.btnsalir.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnsalir.Name = "btnsalir";
            this.btnsalir.Size = new System.Drawing.Size(80, 22);
            this.btnsalir.TabIndex = 4;
            this.btnsalir.Text = "Salir";
            this.btnsalir.UseVisualStyleBackColor = true;
            this.btnsalir.Click += new System.EventHandler(this.btnsalir_Click);
            // 
            // dgvinfo
            // 
            this.dgvinfo.AllowUserToAddRows = false;
            this.dgvinfo.AllowUserToDeleteRows = false;
            this.dgvinfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvinfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column7,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Cantidad,
            this.preciototal,
            this.Column8,
            this.Eliminar});
            this.dgvinfo.Location = new System.Drawing.Point(4, 3);
            this.dgvinfo.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dgvinfo.Name = "dgvinfo";
            this.dgvinfo.ReadOnly = true;
            this.dgvinfo.RowHeadersVisible = false;
            this.dgvinfo.Size = new System.Drawing.Size(652, 145);
            this.dgvinfo.TabIndex = 29;
            this.dgvinfo.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvinfo_CellClick);
            // 
            // Column7
            // 
            this.Column7.HeaderText = "N° Factura";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 82;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Proveedor";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Medicamento";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 130;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Lote";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 80;
            // 
            // Cantidad
            // 
            this.Cantidad.HeaderText = "Cantidad";
            this.Cantidad.Name = "Cantidad";
            this.Cantidad.ReadOnly = true;
            this.Cantidad.Width = 60;
            // 
            // preciototal
            // 
            this.preciototal.HeaderText = "Precio ";
            this.preciototal.Name = "preciototal";
            this.preciototal.ReadOnly = true;
            this.preciototal.Width = 60;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Total";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 60;
            // 
            // Eliminar
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.NullValue = "Eliminar";
            this.Eliminar.DefaultCellStyle = dataGridViewCellStyle1;
            this.Eliminar.HeaderText = "Accion";
            this.Eliminar.Name = "Eliminar";
            this.Eliminar.ReadOnly = true;
            this.Eliminar.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Eliminar.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Eliminar.Width = 70;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnguarda);
            this.panel3.Controls.Add(this.txttotal);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.dgvinfo);
            this.panel3.Enabled = false;
            this.panel3.Location = new System.Drawing.Point(9, 212);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(660, 213);
            this.panel3.TabIndex = 37;
            // 
            // btnguarda
            // 
            this.btnguarda.Location = new System.Drawing.Point(561, 182);
            this.btnguarda.Name = "btnguarda";
            this.btnguarda.Size = new System.Drawing.Size(73, 23);
            this.btnguarda.TabIndex = 0;
            this.btnguarda.Text = "Guardar";
            this.btnguarda.UseVisualStyleBackColor = true;
            this.btnguarda.Click += new System.EventHandler(this.btnguarda_Click);
            // 
            // txttotal
            // 
            this.txttotal.Enabled = false;
            this.txttotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttotal.Location = new System.Drawing.Point(518, 154);
            this.txttotal.Name = "txttotal";
            this.txttotal.Size = new System.Drawing.Size(116, 22);
            this.txttotal.TabIndex = 31;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(473, 157);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 16);
            this.label1.TabIndex = 30;
            this.label1.Text = "Total";
            // 
            // errorP
            // 
            this.errorP.ContainerControl = this;
            // 
            // frmcompra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(669, 470);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.btnsalir);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "frmcompra";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Administrar Compra";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmcompra_FormClosed);
            this.Load += new System.EventHandler(this.frmcompra_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvinfo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numcant)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvinfo)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorP)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnguardar;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cbomed;
        private System.Windows.Forms.ComboBox cbolote;
        private System.Windows.Forms.TextBox txtpre;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbopro;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtnumfactura;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtpfact;
        private System.Windows.Forms.TextBox txtcodcom;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numcant;
        private System.Windows.Forms.DataGridView dgvinfo2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.Button btnsalir;
        private System.Windows.Forms.DataGridView dgvinfo;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnguarda;
        private System.Windows.Forms.TextBox txttotal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ErrorProvider errorP;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DateTimePicker dtpfcad;
        private System.Windows.Forms.DateTimePicker dtpfe;
        private System.Windows.Forms.Label lblfcad;
        private System.Windows.Forms.Label lblfelab;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn preciototal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewButtonColumn Eliminar;

    }
}