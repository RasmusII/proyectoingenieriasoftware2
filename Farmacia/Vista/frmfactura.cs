﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Farmacia.Controlador;

namespace Farmacia.Vista
{
    public partial class frmfactura : Form
    {
        public frmfactura()
        {
            InitializeComponent();
        }

        int fila = -1, pos = 0;
        int id_cli;
        double totalp, iva, subtotal;
        bool flag = true;
        bool flag1 = true;

        private void frmfactura_Load(object sender, EventArgs e)
        {
            codigo();
        }

        private void codigo()
        {
            try
            {
                int num;
                facturadb objF = new facturadb();
                num = objF.traecodigo();
                num++;
                txtnumfact.Text = util.numerofac(num);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error de Datos," + ex.Message, "Farmacia", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtced_Leave(object sender, EventArgs e)
        {
            validaced();
        }

        private void validaced()
        {
            flag1 = true;
            string res = util.validacedula(txtced.Text);
            if (res == "Invalido")
            {
                MessageBox.Show("El numero de cedula es incorrecto");
                txtced.Focus();
            }
            if (txtced.Text != "" && res == "Valido")
            {
                buscarcli(txtced.Text);
            }
            if (flag1 == false)
            {
                buscarcli(txtced.Text);
            }
        }

        private void buscarcli(string txt)
        {
            try
            {
                clientedb objE = new clientedb();
                objE.getcliente().Listacliente = objE.buscacliente(txt, "A");
                if (objE.getcliente().Listacliente.Count > 0)
                {
                    txtcli.Text = objE.getcliente().Listacliente[0].Nombre;
                    txtdir.Text = objE.getcliente().Listacliente[0].Dir_cli;
                    id_cli = objE.getcliente().Listacliente[0].Id_cli;
                    flag1 = true;
                }
                else
                {
                    if (flag1 == true)
                    {
                        frmregistrarcliente frmcli = new frmregistrarcliente(int.Parse(txtced.Text), "F");
                        frmcli.ShowDialog();
                        flag1 = false;
                    }
                    else
                    {
                        txtced.Text = null;
                    }
                }                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error de busqueda " + ex.Message);
            }
        }

        private void txtmed_TextChanged(object sender, EventArgs e)
        {
            if (txtmed.Text != "")
            {
                buscarmed(txtmed.Text);
            }
            else
            {
                dgvlistadomed.Rows.Clear();
                btnagregar.Enabled = true;
                numcant.Minimum = 1;
            }
        }

        private void buscarmed(string txt)
        {
            try
            {
                dgvlistadomed.Rows.Clear();
                medicamentodb objM = new medicamentodb();
                objM.getmed().Listamed = objM.buscamed(txt, "A");
                if (objM.getmed().Listamed.Count > 0)
                {
                    for (int i = 0; i < objM.getmed().Listamed.Count; i++)
                    {
                        fila = 0;
                        dgvlistadomed.Rows.Add(1);
                        dgvlistadomed.Rows[i].Cells[0].Value = objM.getmed().Listamed[i].Nom_med; ;
                        dgvlistadomed.Rows[i].Cells[1].Value = objM.getmed().Listamed[i].Stock; ;
                        dgvlistadomed.Rows[i].Cells[2].Value = objM.getmed().Listamed[i].Pre_ven;
                    }
                    numcant.Maximum = Convert.ToInt16(dgvlistadomed.Rows[fila].Cells[1].Value);
                    double va = objM.getmed().Listamed[fila].Pre_ven;
                    if (numcant.Value == 0 || va == 0)
                    {
                        btnagregar.Enabled = false;
                    }
                }
                else
                {
                    MessageBox.Show("No existen coincidencias");
                    txtmed.Text = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error de busqueda " + ex.Message);
            }
        }

        private void dgvlistadomed_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            fila = dgvlistadomed.CurrentRow.Index;
            numcant.Maximum = Convert.ToInt16(dgvlistadomed.Rows[fila].Cells[1].Value);
            double va = Convert.ToDouble(dgvlistadomed.Rows[fila].Cells[1].Value);
            if (numcant.Value == 0 || va == 0)
            {
                btnagregar.Enabled = false;
            }
        }

        private void btnagregar_Click(object sender, EventArgs e)
        {
            agrega();   
        }

        private void agrega()
        {
            flag = true;
            if (dgvlistadomed.Rows.Count > 0)
            {
                int num = Convert.ToInt16(numcant.Text);
                int cant = Convert.ToInt16(dgvlistadomed.Rows[fila].Cells[1].Value);
                if (num <= cant && cant > 0)
                {
                    if (dgvventa.Rows.Count > 0)
                    {
                        double pr = 0;
                        for (int i = 0; i < pos; i++)
                        {
                            pr += Convert.ToDouble(dgvventa.Rows[i].Cells[3].Value);
                            if (dgvventa.Rows[i].Cells[1].Value.ToString() == dgvlistadomed.Rows[fila].Cells[0].Value.ToString())
                            {
                                string nombre = dgvventa.Rows[i].Cells[1].Value.ToString();
                                if (MessageBox.Show("El producto " + nombre + " ya esta registrado\nDesea aumentar esta cantidad", "Informacion", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                                {
                                    double valortotal;
                                    double pre = Convert.ToDouble(dgvlistadomed.Rows[fila].Cells[2].Value);
                                    int n = Convert.ToInt16(dgvventa.Rows[i].Cells[0].Value) + Convert.ToInt16(numcant.Value);
                                    int cantidad = Convert.ToInt16(numcant.Value);
                                    dgvventa.Rows[i].Cells[0].Value = n;
                                    valortotal = n * pre;
                                    dgvventa.Rows[i].Cells[3].Value = valortotal;
                                    calculos(nombre, cantidad, "N");
                                    flag = false;
                                }
                                else
                                {
                                    flag = false;
                                }
                            }
                        }
                    }
                    if (flag == true)
                    {
                        adicciona();
                    }
                }
            }
            else
            {
                MessageBox.Show("Faltan datos");
            }
            txtmed.Text = null;
            numcant.Value = 1;
            txtmed.Focus();   
        }

        private void adicciona()
        {
            panel3.Enabled = true;
            int cant;
            string nom= dgvlistadomed.Rows[fila].Cells[0].Value.ToString();
            double tot, valortotal;
            dgvventa.Rows.Add(1);
            dgvventa.Rows[pos].Cells[0].Value = numcant.Value;
            dgvventa.Rows[pos].Cells[1].Value = nom;
            dgvventa.Rows[pos].Cells[2].Value = dgvlistadomed.Rows[fila].Cells[2].Value;
            cant = Convert.ToInt16(numcant.Value);
            tot = Convert.ToDouble(dgvlistadomed.Rows[fila].Cells[2].Value);
            valortotal = cant * tot;
            dgvventa.Rows[pos].Cells[3].Value = valortotal;
            calculos(nom, cant, "N");
            pos++;
        }

        private void actualizatotal()
        {
            double pr = 0;
            foreach (DataGridViewRow row in dgvventa.Rows)
            {
                pr += Convert.ToDouble(row.Cells["Column4"].Value);
            }
            subtotal = pr;
        }

        private void calculos(string N, int C, string opc)
        {
            actualizatotal();
            facturadb objF = new facturadb();
            int resp, stock;
            iva = subtotal * 0.12;
            totalp = subtotal + iva;
            txtsubt.Text = subtotal.ToString();
            txtiva.Text = iva.ToString();
            txttotal.Text = totalp.ToString();            
            if (opc == "N")
            {
                stock = Convert.ToInt16(dgvlistadomed.Rows[fila].Cells[1].Value) - C;
                resp = objF.actualizastock(stock, "D", N);
            }
            else
            {
                stock = C;
                resp = objF.actualizastock(stock, "E", N);
            }
        }

        private void dgvventa_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dgvventa.Columns[e.ColumnIndex].Name == "Eliminar")
            {
                fila = dgvventa.CurrentRow.Index;
                string nom = dgvventa.Rows[fila].Cells[1].Value.ToString();
                int ca = Convert.ToInt16(dgvventa.Rows[fila].Cells[0].Value.ToString());
                dgvventa.Rows.Remove(dgvventa.CurrentRow);
                calculos(nom, ca, "E");
                pos--;
            }
            if (pos == 0)
            {
                panel3.Enabled = false; 
                txtsubt.Text = null;
                txtiva.Text = null;
                txttotal.Text = null;
            }
        }

        private void btnguardar_Click(object sender, EventArgs e)
        {
            if (txtvalor.Text != "")
            {
                if (Convert.ToDouble(txttotal.Text) <= Convert.ToDouble(txtvalor.Text))
                {
                    if (txtced.Text != "")
                    {
                        guardafactura();
                        frmreportes objR = new frmreportes(txtnumfact.Text);
                        objR.Show();
                        dgvlistadomed.Rows.Clear();
                        txtced.Text = null;
                        txtdir.Text = null;
                        txtcli.Text = null;
                        txtvalor.Text = null;
                        txtsubt.Text = null;
                        txttotal.Text = null;
                        txtiva.Text = null;
                        codigo();
                        dgvventa.Rows.Clear();
                        
                    }
                    else
                    {
                        MessageBox.Show("Falta ingresar el cliente");
                    }
                }
                else
                {
                    MessageBox.Show("El valor con el que cancela es menor a lo que debe pagar");
                    txtvalor.Text = null;
                    txtvalor.Focus();
                }
            }
            else
            {
                MessageBox.Show("Faltan datos");
            }
        }

        private void guardafactura()
        {
            try
            {
                int resp = 0;
                facturadb objF = new facturadb();
                detallefacturadb objDF = new detallefacturadb();

                objDF.getdetalle().Num_fact = txtnumfact.Text;
                for (int i = 0; i < pos; i++)
                {
                    objDF.getdetalle().Cantidad = int.Parse(dgvventa.Rows[i].Cells[0].Value.ToString());
                    objDF.getdetalle().Des_fact = dgvventa.Rows[i].Cells[1].Value.ToString();
                    objDF.getdetalle().Precio = Convert.ToDouble(dgvventa.Rows[i].Cells[2].Value.ToString());
                    objDF.getdetalle().Total = Convert.ToDouble(dgvventa.Rows[i].Cells[3].Value.ToString());

                    resp = objDF.insertadetallefactura(objDF.getdetalle());
                    if (resp == 0)
                    {
                        MessageBox.Show("No se ingreso los datos en el detalle factura");
                    }
                }
                resp = 0;
                objF.getfactura().Num_fact = txtnumfact.Text;
                objF.getfactura().Fec_emi = util.girafecha(dtp1.Value.ToShortDateString());
                objF.getfactura().Id_cli = id_cli;                
                objF.getfactura().Subtotal = Convert.ToDouble(txtsubt.Text);
                objF.getfactura().Iva = Convert.ToDouble(txtiva.Text);                
                objF.getfactura().Total = Convert.ToDouble(txttotal.Text);
                objF.getfactura().Id_ger = "A001";
                resp = objF.insertafactura(objF.getfactura());
                if (resp == 0)
                {
                    MessageBox.Show("No se han ingresado datos de la factura");
                }
                double to = Convert.ToDouble(txttotal.Text);
                double pa = Convert.ToDouble(txtvalor.Text);
                double T= pa - to;
                MessageBox.Show(T+"","CAMBIO",MessageBoxButtons.OK,MessageBoxIcon.Information);
                MessageBox.Show("Datos almacenados");
                pos=0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }
        }

        private void txtced_KeyPress(object sender, KeyPressEventArgs e)
        {
            char letra = e.KeyChar;
            if (!char.IsDigit(letra) && letra != 8 && letra != 13 && letra != 32)
                e.Handled = true;
        }

        private void frmfactura_FormClosed(object sender, FormClosedEventArgs e)
        {
            foreach (DataGridViewRow row in dgvventa.Rows)
            {
                facturadb objF = new facturadb();
                int resp = 0;
                int ca = Convert.ToInt16(row.Cells["Column1"].Value);
                string nom = row.Cells["Column2"].Value.ToString();
                resp = objF.actualizastock(ca, "E", nom);
            }
            util.mustrapantallas("F", 1);
        }

        private void txtmed_KeyPress(object sender, KeyPressEventArgs e)
        {
            char letra = e.KeyChar;
            if (!char.IsLetterOrDigit(letra) && letra != 8 && letra != 13 && letra != 32)
                e.Handled = true;
        }
    }
}
