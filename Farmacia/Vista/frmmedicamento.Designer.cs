﻿namespace Farmacia.Vista
{
    partial class frmmedicamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnmodificar = new System.Windows.Forms.Button();
            this.btnsalir = new System.Windows.Forms.Button();
            this.dgvlistadomed = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cbotipomed = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtprecioc = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtpreciov = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btncancelar = new System.Windows.Forms.Button();
            this.btnguardar = new System.Windows.Forms.Button();
            this.txtstock = new System.Windows.Forms.TextBox();
            this.txtnommed = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnnuevo = new System.Windows.Forms.Button();
            this.txtbuscar = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btndes = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.chkdes = new System.Windows.Forms.CheckBox();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgvlistadomed)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            this.SuspendLayout();
            // 
            // btnmodificar
            // 
            this.btnmodificar.Location = new System.Drawing.Point(81, 3);
            this.btnmodificar.Name = "btnmodificar";
            this.btnmodificar.Size = new System.Drawing.Size(72, 24);
            this.btnmodificar.TabIndex = 23;
            this.btnmodificar.Text = "Modificar";
            this.btnmodificar.UseVisualStyleBackColor = true;
            this.btnmodificar.Click += new System.EventHandler(this.btnmodificar_Click);
            // 
            // btnsalir
            // 
            this.btnsalir.Location = new System.Drawing.Point(346, 355);
            this.btnsalir.Name = "btnsalir";
            this.btnsalir.Size = new System.Drawing.Size(75, 23);
            this.btnsalir.TabIndex = 22;
            this.btnsalir.Text = "Salir";
            this.btnsalir.UseVisualStyleBackColor = true;
            this.btnsalir.Click += new System.EventHandler(this.btnsalir_Click);
            // 
            // dgvlistadomed
            // 
            this.dgvlistadomed.AllowUserToAddRows = false;
            this.dgvlistadomed.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvlistadomed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvlistadomed.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.dgvlistadomed.Location = new System.Drawing.Point(12, 58);
            this.dgvlistadomed.Name = "dgvlistadomed";
            this.dgvlistadomed.ReadOnly = true;
            this.dgvlistadomed.RowHeadersVisible = false;
            this.dgvlistadomed.Size = new System.Drawing.Size(405, 111);
            this.dgvlistadomed.TabIndex = 21;
            this.dgvlistadomed.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvlistadomed_CellClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "id_medicamento";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 90;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Nombre_medicamento";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 200;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Stock";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 45;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "P.Venta";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 55;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(9, 209);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(154, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Nuevo/Modificar Medicamento";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.cbotipomed);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.txtprecioc);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtpreciov);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.btncancelar);
            this.panel1.Controls.Add(this.btnguardar);
            this.panel1.Controls.Add(this.txtstock);
            this.panel1.Controls.Add(this.txtnommed);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Enabled = false;
            this.panel1.Location = new System.Drawing.Point(12, 227);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(427, 122);
            this.panel1.TabIndex = 18;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(134, 60);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(14, 16);
            this.label10.TabIndex = 33;
            this.label10.Text = "*";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(134, 34);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(14, 16);
            this.label12.TabIndex = 31;
            this.label12.Text = "*";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(134, 8);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(14, 16);
            this.label13.TabIndex = 30;
            this.label13.Text = "*";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 13);
            this.label8.TabIndex = 29;
            this.label8.Text = "Tipo del medicamento";
            // 
            // cbotipomed
            // 
            this.cbotipomed.FormattingEnabled = true;
            this.cbotipomed.Items.AddRange(new object[] {
            "Aerosol",
            "Cápsula",
            "Colirio",
            "Crema",
            "Elixir",
            "Emplastos",
            "Enema",
            "Grageas",
            "Inhaladores",
            "Jarabe",
            "Linimentos",
            "Locion",
            "Óvulos",
            "Pastas",
            "Píldoras",
            "Pomadas",
            "Polvos",
            "Soluciones",
            "Supositorios",
            "Tabletas o comprimidos"});
            this.cbotipomed.Location = new System.Drawing.Point(149, 3);
            this.cbotipomed.Name = "cbotipomed";
            this.cbotipomed.Size = new System.Drawing.Size(252, 21);
            this.cbotipomed.TabIndex = 0;
            this.cbotipomed.Text = "Seleccione....";
            this.cbotipomed.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbotipomed_KeyPress);
            this.cbotipomed.Leave += new System.EventHandler(this.cbotipomed_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(226, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 13);
            this.label7.TabIndex = 28;
            this.label7.Text = "$";
            // 
            // txtprecioc
            // 
            this.txtprecioc.Enabled = false;
            this.txtprecioc.Location = new System.Drawing.Point(149, 82);
            this.txtprecioc.Name = "txtprecioc";
            this.txtprecioc.Size = new System.Drawing.Size(83, 20);
            this.txtprecioc.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(46, 85);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Precio de compra";
            // 
            // txtpreciov
            // 
            this.txtpreciov.Location = new System.Drawing.Point(149, 56);
            this.txtpreciov.Name = "txtpreciov";
            this.txtpreciov.Size = new System.Drawing.Size(76, 20);
            this.txtpreciov.TabIndex = 2;
            this.txtpreciov.TextChanged += new System.EventHandler(this.txtpreciov_TextChanged);
            this.txtpreciov.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtpreciov_KeyPress);
            this.txtpreciov.Leave += new System.EventHandler(this.txtpreciov_Leave);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(54, 59);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Precio de venta";
            // 
            // btncancelar
            // 
            this.btncancelar.Location = new System.Drawing.Point(328, 82);
            this.btncancelar.Name = "btncancelar";
            this.btncancelar.Size = new System.Drawing.Size(75, 23);
            this.btncancelar.TabIndex = 7;
            this.btncancelar.Text = "Cancelar";
            this.btncancelar.UseVisualStyleBackColor = true;
            this.btncancelar.Click += new System.EventHandler(this.btncancelar_Click);
            // 
            // btnguardar
            // 
            this.btnguardar.Location = new System.Drawing.Point(247, 82);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.Size = new System.Drawing.Size(75, 23);
            this.btnguardar.TabIndex = 4;
            this.btnguardar.Text = "Guardar";
            this.btnguardar.UseVisualStyleBackColor = true;
            this.btnguardar.Click += new System.EventHandler(this.btnguardar_Click);
            // 
            // txtstock
            // 
            this.txtstock.Enabled = false;
            this.txtstock.Location = new System.Drawing.Point(311, 56);
            this.txtstock.Name = "txtstock";
            this.txtstock.Size = new System.Drawing.Size(90, 20);
            this.txtstock.TabIndex = 5;
            // 
            // txtnommed
            // 
            this.txtnommed.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtnommed.Location = new System.Drawing.Point(149, 30);
            this.txtnommed.MaxLength = 50;
            this.txtnommed.Name = "txtnommed";
            this.txtnommed.Size = new System.Drawing.Size(252, 20);
            this.txtnommed.TabIndex = 1;
            this.txtnommed.TextChanged += new System.EventHandler(this.txtnommed_TextChanged);
            this.txtnommed.Leave += new System.EventHandler(this.txtnommed_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(127, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Nombre del medicamento";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(269, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Stock";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Listado del medicamento";
            // 
            // btnnuevo
            // 
            this.btnnuevo.Location = new System.Drawing.Point(3, 3);
            this.btnnuevo.Name = "btnnuevo";
            this.btnnuevo.Size = new System.Drawing.Size(72, 24);
            this.btnnuevo.TabIndex = 0;
            this.btnnuevo.Text = "Registrar";
            this.btnnuevo.UseVisualStyleBackColor = true;
            this.btnnuevo.Click += new System.EventHandler(this.btnnuevo_Click);
            // 
            // txtbuscar
            // 
            this.txtbuscar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtbuscar.Location = new System.Drawing.Point(89, 6);
            this.txtbuscar.MaxLength = 50;
            this.txtbuscar.Name = "txtbuscar";
            this.txtbuscar.Size = new System.Drawing.Size(212, 20);
            this.txtbuscar.TabIndex = 12;
            this.txtbuscar.TextChanged += new System.EventHandler(this.txtbuscar_TextChanged);
            this.txtbuscar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtbuscar_KeyPress);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btndes);
            this.panel2.Controls.Add(this.btnmodificar);
            this.panel2.Controls.Add(this.btnnuevo);
            this.panel2.Location = new System.Drawing.Point(12, 172);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(239, 31);
            this.panel2.TabIndex = 24;
            // 
            // btndes
            // 
            this.btndes.Location = new System.Drawing.Point(159, 3);
            this.btndes.Name = "btndes";
            this.btndes.Size = new System.Drawing.Size(72, 24);
            this.btndes.TabIndex = 24;
            this.btndes.Text = "Desactivar";
            this.btndes.UseVisualStyleBackColor = true;
            this.btndes.Click += new System.EventHandler(this.btndes_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "Medicamento";
            // 
            // chkdes
            // 
            this.chkdes.AutoSize = true;
            this.chkdes.Location = new System.Drawing.Point(326, 38);
            this.chkdes.Name = "chkdes";
            this.chkdes.Size = new System.Drawing.Size(91, 17);
            this.chkdes.TabIndex = 26;
            this.chkdes.Text = "Desactivados";
            this.chkdes.UseVisualStyleBackColor = true;
            this.chkdes.CheckedChanged += new System.EventHandler(this.chkdes_CheckedChanged);
            // 
            // error
            // 
            this.error.ContainerControl = this;
            // 
            // frmmedicamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 388);
            this.Controls.Add(this.chkdes);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnsalir);
            this.Controls.Add(this.dgvlistadomed);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtbuscar);
            this.Name = "frmmedicamento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Administrar Medicamento";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmmedicamento_FormClosed);
            this.Load += new System.EventHandler(this.frmmedicamento_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvlistadomed)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnmodificar;
        private System.Windows.Forms.Button btnsalir;
        private System.Windows.Forms.DataGridView dgvlistadomed;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtpreciov;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btncancelar;
        private System.Windows.Forms.Button btnguardar;
        private System.Windows.Forms.TextBox txtstock;
        private System.Windows.Forms.TextBox txtnommed;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnnuevo;
        private System.Windows.Forms.TextBox txtbuscar;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btndes;
        private System.Windows.Forms.TextBox txtprecioc;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkdes;
        private System.Windows.Forms.ComboBox cbotipomed;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ErrorProvider error;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
    }
}