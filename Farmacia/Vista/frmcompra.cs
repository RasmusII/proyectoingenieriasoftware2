﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Farmacia.Controlador;

namespace Farmacia.Vista
{
    public partial class frmcompra : Form
    {
        public frmcompra()
        {
            InitializeComponent();
        }

        int recp=0, pos=0, cod, recmed=0, recl=0;
        double totalp;
        bool flag = true;
        
        private void btnguardar_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void guardar()
        {
            flag = true;
            if (cbopro.Text != "Seleccione..." && cbomed.Text != "Seleccione..." && txtnumfactura.Text != "" && txtpre.Text != "")
            {
                if (dgvinfo.Rows.Count > 0)
                {
                    for (int i = 0; i < pos; i++)
                    {
                        double pr = Convert.ToDouble(txtpre.Text);
                        string nombre = dgvinfo.Rows[i].Cells[2].Value.ToString();
                        if (nombre == cbomed.Text)
                        {
                            if (MessageBox.Show("El producto " + nombre + " ya esta registrado, y esta con un precio de " + pr + " $\nDesea aumentar esta cantidad y cambiar el precio de compra", "Informacion", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                            {
                                int n = Convert.ToInt16(dgvinfo.Rows[i].Cells[4].Value) + Convert.ToInt16(numcant.Value);
                                dgvinfo.Rows[i].Cells[4].Value = n;
                                dgvinfo.Rows[i].Cells[5].Value = pr;
                                totalp = n * pr;
                                dgvinfo.Rows[i].Cells[6].Value = totalp;
                                actualizatotal();
                                flag = false;
                            }
                        }
                    }
                }
                if (flag == true)
                {
                    adicciona();
                }
                txtpre.Text = null;
                numcant.Value = 1;
                cbomed.Text = "Seleccione...";
                cbolote.Text = "";
                panel3.Enabled = true;
                numcant.Value = 1;
                lblfcad.Visible = false;
                lblfelab.Visible = false;
                dtpfe.Visible = false;
                dtpfcad.Visible = false;                
            }
            else
            {
                enviarerror();
            }
        }

        private void actualizatotal()
        {
            double pr = 0;
            foreach (DataGridViewRow row in dgvinfo.Rows)
            {
                pr += Convert.ToDouble(row.Cells["Column8"].Value);
            }
            txttotal.Text = pr.ToString();
        }

        public void enviarerror()
        {
            if (cbomed.Text == "Seleccione...")
            {
                errorP.SetError(cbomed, "Debe selecciona este campo obligatoriamente");
            }
            if (cbopro.Text == "Seleccione...")
            {
                errorP.SetError(cbopro, "Debe selecciona este campo obligatoriamente");
            }
            if (txtpre.Text == "")
            {
                errorP.SetError(txtpre, "Debe ingresar el precio obligatoriamente");
            }
            if (txtnumfactura.Text == "")
            {
                errorP.SetError(txtnumfactura, "Debe ingresar el numero de factura obligatoriamente");
            }
        }

        private void adicciona()
        {
            int cant;
            double tot;
            dgvinfo.Rows.Add(1);
            dgvinfo.Rows[pos].Cells[0].Value = txtnumfactura.Text;
            dgvinfo.Rows[pos].Cells[1].Value = cbopro.Text;
            dgvinfo.Rows[pos].Cells[2].Value = cbomed.Text;
            dgvinfo.Rows[pos].Cells[3].Value = cbolote.Text;
            dgvinfo.Rows[pos].Cells[4].Value = numcant.Value;
            dgvinfo.Rows[pos].Cells[5].Value = txtpre.Text;
            cant =Convert.ToInt16(numcant.Value);
            tot = Convert.ToDouble(txtpre.Text);
            totalp = cant * tot;
            dgvinfo.Rows[pos].Cells[6].Value = totalp;
            actualizatotal();            
            pos++;
        }

        private void frmcompra_Load(object sender, EventArgs e)
        {
            compradb objC = new compradb();
            llenaproveedor(cbopro);
            llenacbomed(cbomed);
            cbolote.Text = "";
            int nn = objC.generacodigo();
            txtcodcom.Text = Convert.ToString(++nn);
        }

        private void llenaproveedor(ComboBox cbo)
        {
            try
            {
                proveedordb objP = new proveedordb();
                objP.getemp().Listaempresa = objP.traedatos("A");
                if (objP.getemp().Listaempresa.Count == 0)
                {
                    MessageBox.Show("Por el momento no cuenta con ningun proveedor\nIngrese antes un proveedor para continuar con la compra");
                    panel2.Enabled = false;
                }
                else
                {
                    recp = 1;
                    cbo.DisplayMember = "Nom_emp";
                    cbo.ValueMember = "Ruc_emp";                    
                    cbo.DataSource = objP.getemp().Listaempresa;
                    cbopro.Text = "Seleccione...";                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }
        } //llena combobox proveedor

        private void llenacbomed(ComboBox cbo)
        {
            try
            {
                medicamentodb objM = new medicamentodb();
                //objM.getmed().Listamed = objM.traecuentacbo("A");
                objM.getmed().Listamed = objM.traemed("A");
                if (objM.getmed().Listamed.Count == 0)
                {
                    MessageBox.Show("No existen registros de medicamentos comprados\nIngrese primero los medicamentos que haya comprado");
                    panel2.Enabled = false;
                }
                recmed = 1;
                cbo.DisplayMember = "Nom_med";
                cbo.ValueMember = "Id_med";
                cbo.DataSource = objM.getmed().Listamed;
                cbomed.Text = "Seleccione...";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en la presentacion de datos " + ex.Message);
            }
        } //llena combobox medicamento

        private void cbomed_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (recmed == 0 && cbomed.Text != "Seleccione...")
            {
                llenalote(cbolote);
            }
            recmed = 0;
        } 

        private void llenalote(ComboBox cbo)
        {
            try
            {
                lotedb objE = new lotedb();
                objE.getlote().Listalote = objE.traelotes(int.Parse(cbomed.SelectedValue.ToString()));
                if (objE.getlote().Listalote.Count == 0)
                {
                    MessageBox.Show("No existen datos de algun lote\nRegistre el lote del medicamento antes de generar la compra");
                    panel2.Enabled = false;
                }
                else
                {
                    recl = 1;
                    cbo.DisplayMember = "Id_lote";
                    cbo.ValueMember = "Id_lote";
                    cbo.DataSource = objE.getlote().Listalote;
                    cbolote.Text = "Selecione...";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }
        } //llena combobox lote

        private void cbopro_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (recp == 0 && cbopro.Text != "Seleccione...")
            {
                buscapro();
            }
            txtnumfactura.Text = null;   
            recp = 0;
        }

        private void buscapro()
        {
            try
            {
                dgvinfo2.Rows.Clear();
                proveedordb objPr = new proveedordb();
                objPr.getemp().Listaempresa = objPr.buscaemp(cbopro.SelectedValue.ToString(), "A");
                if (objPr.getemp().Listaempresa.Count == 0)
                {
                    MessageBox.Show("No existen datos");
                }
                else
                {
                    dgvinfo2.Rows.Add(1);
                    dgvinfo2.Rows[0].Cells[0].Value = objPr.getemp().Listaempresa[0].Nom_emp;
                    dgvinfo2.Rows[0].Cells[1].Value = objPr.getemp().Listaempresa[0].Tel_emp;
                    dgvinfo2.Rows[0].Cells[2].Value = objPr.getemp().Listaempresa[0].Dir_emp;
                    cod = objPr.getemp().Listaempresa[0].Id_emp;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }
        }
        
        private void dgvinfo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dgvinfo.Columns[e.ColumnIndex].Name == "Eliminar")
            {
                dgvinfo.Rows.Remove(dgvinfo.CurrentRow);
                actualizatotal();
                pos--;
            }
        }
        
        private void btnguarda_Click(object sender, EventArgs e)
        {
            guardacompra();
            lblfcad.Visible = false;
            lblfelab.Visible = false;
            dtpfcad.Visible = false;
            dtpfe.Visible = false;
            dgvinfo2.Rows.Clear();
            cbopro.Text = "Seleccione...";
            txtnumfactura.Text = null;
            txttotal.Text = null;
            compradb objC = new compradb();
            int nn = objC.generacodigo();
            txtcodcom.Text = Convert.ToString(++nn);
            dgvinfo.Rows.Clear();
        }

        private void guardacompra()
        {
            try
            {
                int resp=0;
                lotecompradb objLC = new lotecompradb();
                compradb objC = new compradb();
                proveedordb objP = new proveedordb();
                medicamentodb objM = new medicamentodb();

                objC.getcompra().Id_com = int.Parse(txtcodcom.Text);
                objC.getcompra().Id_empr = cod;
                objC.getcompra().Num_fact= int.Parse(txtcodcom.Text);
                objC.getcompra().Fec_emi=util.girafecha(dtpfact.Value.ToShortDateString());
                objC.getcompra().Total=txttotal.Text;
                resp = objC.insertacompra(objC.getcompra());
                if (resp == 0)
                {
                    MessageBox.Show("No se han ingresado datos de la compra");
                } 
                resp = 0;
                objLC.getlotecom().Id_compra=int.Parse(txtcodcom.Text);               
                
                for (int i = 0; i < pos; i++)
                {
                    objLC.getlotecom().Cod_lot = dgvinfo.Rows[i].Cells[3].Value.ToString();
                    objLC.getlotecom().Cant = int.Parse(dgvinfo.Rows[i].Cells[4].Value.ToString());
                    objLC.getlotecom().Precio = dgvinfo.Rows[i].Cells[5].Value.ToString();
                    resp = objLC.insertalotecompra(objLC.getlotecom());
                    if (resp == 0)
                    {
                        MessageBox.Show("No se ingreso los datos en lote compra");
                    }
                }
                resp = 0;
                for (int i = 0; i < pos; i++)
                {
                    objM.getmed().Nom_med = dgvinfo.Rows[i].Cells[2].Value.ToString();
                    objM.getmed().Stock = int.Parse(dgvinfo.Rows[i].Cells[4].Value.ToString());
                    objM.getmed().Pre_com = dgvinfo.Rows[i].Cells[5].Value.ToString();
                    resp = objM.insertastock(objM.getmed());
                    if (resp == 0)
                    {
                        MessageBox.Show("No se ha modificado el stock");
                    }
                }
                MessageBox.Show("Datos almacenados");
                pos = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            util.mustrapantallas("Co", 1);
            Close();
        }

        private void txtpre_KeyPress(object sender, KeyPressEventArgs e)
        {
            char letra = e.KeyChar;
            if (!char.IsDigit(letra) && letra != 13 && letra != 8 && letra != 46)
                e.Handled = true;
            if (letra == 46 && txtpre.Text.Contains('.'))
                e.Handled = true;
        }

        private void cbopro_Leave(object sender, EventArgs e)
        {
            if (cbopro.Text == "Seleccione...")
            {
                errorP.SetError(cbopro, "Debe selecciona este campo obligatoriamente");
            }
            else
            {
                errorP.SetError(cbopro, "");
            }
        }

        private void cbomed_Leave(object sender, EventArgs e)
        {
            if (cbomed.Text == "Seleccione...")
            {
                errorP.SetError(cbomed, "Debe selecciona este campo obligatoriamente");
            }
            else
            {
                errorP.SetError(cbomed, "");
            }
        }

        private void txtpre_Leave(object sender, EventArgs e)
        {
            if (txtpre.Text == "")
            {
                errorP.SetError(txtpre, "Debe ingresar el precio obligatoriamente");
            }
        }

        private void txtnumfactura_Leave(object sender, EventArgs e)
        {
            if (txtnumfactura.Text == "")
            {
                errorP.SetError(txtnumfactura, "Debe ingresar el numero de factura obligatoriamente");
            }
            else if (cbopro.Text != "Seleccione...")
            {
                buscanumfact();
            }
            else
            {
                MessageBox.Show("Debe seleccionar un proveedor");
            }
        }

        private void buscanumfact()
        {
            int resp = 0;
            try
            {
                compradb objC = new compradb();
                resp = objC.traenumfac(cod, txtnumfactura.Text);
                if (resp>0)
                {
                    MessageBox.Show("El numero de factura ya existe");
                    txtnumfactura.Text = null;
                    txtnumfactura.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error "+ex.Message);
            }
        }

        private void txtnumfactura_KeyPress(object sender, KeyPressEventArgs e)
        {
            char letra = e.KeyChar;
            if (!char.IsDigit(letra) && letra != 13 && letra != 8)
                e.Handled = true;
        }

        private void txtnumfactura_TextChanged(object sender, EventArgs e)
        {
            if (txtnumfactura.Text != "")
            {
                errorP.SetError(txtnumfactura, "");
            }
        }

        private void txtpre_TextChanged(object sender, EventArgs e)
        {
            if (txtpre.Text != "")
            {
                errorP.SetError(txtpre, "");
            }
        }

        private void cbolote_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (recl == 0 && cbolote.Text != "Seleccione...")
            {
                muestrafechas();
            }
            recl = 0;
        }

        private void muestrafechas()
        {
            try
            {
                lotedb objL = new lotedb();
                objL.setlote(objL.traelote(cbolote.Text));
                if (objL.getlote().Listalote.Count >= 0)
                {
                    lblfcad.Visible = true;
                    lblfelab.Visible = true;
                    dtpfcad.Visible = true;
                    dtpfe.Visible = true;
                    dtpfe.Value = DateTime.Parse(objL.getlote().Fec_ela);
                    dtpfcad.Value = DateTime.Parse(objL.getlote().Fec_cad);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error de busqueda " + ex.Message);
            }
        }

        private void cbopro_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cbomed_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cbolote_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void frmcompra_FormClosed(object sender, FormClosedEventArgs e)
        {
            util.mustrapantallas("Co", 1);
        }
    }
}
