﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Farmacia.Controlador;

namespace Farmacia.Vista
{
    public partial class frmregistrarcliente : Form
    {
        public frmregistrarcliente(int cedula, string est)
        {
            InitializeComponent();
            this.cedula=cedula;
            this.est = est;
        }

        int cedula;//Variable que guarda el numero de la cedula
        string est = "";//Variable que guarda si se va a registrar un nuevo cliente o se va a modificar
        string estado, res = "";//Variable estado guarda si es nuevo proveedor o lo va a modificar, y resp guarda la informacion de la cedula(valido, invalido)
        //Evento que verifica si el estado sera nuevo o a modificar
        private void frmregistrarcliente_Load(object sender, EventArgs e)
        {
            if (est=="N"|| est=="F")
            {
                if (cedula != 0)
                {
                    txtced.Text = cedula.ToString();
                }
                estado = "N";
            }
            if (cedula != 0 && est=="M")
            {
                estado = "E";
                modificar(cedula);
            }            
        }
        //Evento para salir de la pantalla
        private void btnsalir_Click(object sender, EventArgs e)
        {
            Close();
        }
        //Evento que verifica si se introducido informacion y manda a guardar o modificar la informacion 
        private void btnguardar_Click(object sender, EventArgs e)
        {
            if (txtced.Text != "" && txtape.Text != "" && txtnom.Text != "" && txtdir.Text != "")
            {
                if (estado == "N")
                {
                    adicciona();
                }
                if (estado == "E")
                {
                    editar();
                }
                if (est == "F")
                {
                    frmfactura frmf = new frmfactura();
                    Close();
                }
                else
                {
                    frmcliente frmcli = new frmcliente();
                    this.Close();
                    frmcli.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("Faltan datos");
                validarcampo();
            }
            //if (res == "Valido")
            //{
               
            //}
        }
        //Metodo que verifica que informacion no ha sido ingresada
        private void validarcampo()
        {
            if (txtnom.Text == "")
            {
                error.SetError(txtnom, "Ingrese el nombre");
            }
            if (txtced.Text == "")
            {
                error.SetError(txtced, "Ingrese la cedula");
            }
            if (txtape.Text == "")
            {
                error.SetError(txtape, "Ingrese el apellido");
            }
            if (txtdir.Text == "")
            {
                error.SetError(txtdir, "Ingrese el direccion");
            }
        }
        //Metodo que envia la informacion al Modelo.cliente para guardarla
        private void adicciona()
        {
            try
            {
                clientedb objC = new clientedb();
                int resp;
                llenacliente(objC);
                resp = objC.insertacliente(objC.getcliente());
                if (resp == 0)
                {
                    MessageBox.Show("No se han ingresado datos");
                }
                else
                {
                    MessageBox.Show("Cliente ingresado");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error  " + ex.Message);
            }
        }
        //Metodo que llena la informacion en la clase clientedb
        private clientedb llenacliente(clientedb cli)
        {
            cli.getcliente().Nom_cli = txtnom.Text;
            cli.getcliente().Ape_cli = txtape.Text;
            cli.getcliente().Ced_cli = int.Parse(txtced.Text);
            cli.getcliente().Dir_cli = txtdir.Text;
            cli.getcliente().Tel_cli = txttel.Text;
            cli.getcliente().Cor_cli = txtcor.Text;
            cli.getcliente().Est_cli = "A";
            return cli;
        }
        //Evento para volver a la pantalla Proveedor o Factura segun el estado
        private void btncancelar_Click(object sender, EventArgs e)
        {
            if (est == "F")
            {
                this.Close();
            }
            else
            {
                frmcliente frmcli = new frmcliente();
                Close();
                frmcli.ShowDialog();
            }
        }
        //Metodo que trae los datos del cliente para modificarlos
        private void modificar(int ced)
        {
            try
            {
                clientedb objC = new clientedb();
                objC.setcliente(objC.traecliente(ced.ToString()));
                if (objC.getcliente().Ced_cli.ToString() == "")
                {
                    MessageBox.Show("No existe departamento");
                }
                else
                {
                    txtced.Text = objC.getcliente().Ced_cli.ToString();
                    txtape.Text = objC.getcliente().Ape_cli;
                    txtnom.Text = objC.getcliente().Nom_cli;
                    txtdir.Text = objC.getcliente().Dir_cli;
                    txtcor.Text = objC.getcliente().Cor_cli;
                    txttel.Text = objC.getcliente().Tel_cli.ToString();
                    txtced.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en los datos, " + ex.Message);
            }
        }
        //Metodo que envia la informacion al Modelo.cliente para modificarla
        private void editar()
        {
            try
            {
                clientedb objC = new clientedb();
                int resp;
                llenacliente(objC);
                resp = objC.Actualizacliente(objC.getcliente());
                if (resp == 0)
                {
                    MessageBox.Show("No se modifico cliente");
                }
                else
                {
                    MessageBox.Show("Cliente modificado");
                    estado = "";
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }
        }
        //Evento para validar solo el ingreso de numeros
        private void validanumeros(object sender, KeyPressEventArgs e)
        {
            char letra = e.KeyChar;
            if (!char.IsDigit(letra) && letra != 8 && letra != 13 && letra != 32)
                e.Handled = true;
        }
        //Evento para validar solo el ingreso de letras
        private void validaletras(object sender, KeyPressEventArgs e)
        {
            char letra = e.KeyChar;
            if (!char.IsLetter(letra) && letra != 32 && letra != 8 && letra != 13)
                e.Handled = true;
        }
        //Evento que valida y manda a buscar la cedula al momento de cambiar el foco o envia muestra un mensaje de error si no ha ingresadomla cedula
        private void txtced_Leave(object sender, EventArgs e)
        {
            res = util.validacedula(txtced.Text);
            if (res == "Invalido")
            {
                MessageBox.Show("El numero de cedula es incorrecto");
                txtced.Focus();
            }
            if (txtced.Text != "")
            {
                buscar(txtced.Text);
            }
            else
            {
                error.SetError(txtced, "El numero de cedula es obligatorio");
            }
        }
        //Metodo para la busqueda de un cliente
        private void buscar(string txt)
        {
            try
            {
                clientedb objE = new clientedb();
                objE.getcliente().Listacliente = objE.buscacliente(txt,"T");
                if (objE.getcliente().Listacliente.Count > 0)
                {
                    string es = objE.getcliente().Listacliente[0].Est_cli;
                    if (es == "P")
                    {
                        MessageBox.Show("El cliente ya existe pero esta desactivado");
                    }
                    else
                    {
                        MessageBox.Show("El cliente ya existe");
                    }
                    frmcliente frmcli = new frmcliente();
                    Close();
                    frmcli.ShowDialog();                         
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error de busqueda " + ex.Message);
            }
        }
        //Evento que envia el mensaje del errorprovider y manda a validar el correo
        private void txtcor_Leave(object sender, EventArgs e)
        {
            if (txtcor.Text != "")
            {
                validacor();
            }
        }
        //Metodo para mandar a validar el correo
        public void validacor()
        {
            string res = "";
            if (txtcor.Text != "")
            {
                if (txtcor.TextLength > 0 && txtcor.Text.Contains('@'))
                {
                    res = util.validacorreo(txtcor.Text);
                    if (res == "Invalido")
                    {
                        MessageBox.Show("Mal ingresado el correo");
                        txtcor.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Mal ingresado el correo");
                    txtcor.Focus();
                }
            }
        }
        //Evento que envia el mensaje del errorprovidera null si ha escrito algo
        private void txtced_TextChanged(object sender, EventArgs e)
        {
            if (txtced.Text != "")
            {
                error.SetError(txtced, "");
            }
        }
        //Evento que envia el mensaje del errorprovider si no se ha escrito una vez cambia el foco
        private void txtape_Leave(object sender, EventArgs e)
        {
            if (txtape.Text == "")
            {
                error.SetError(txtape, "Debe ingresar el apellido obligatoriamente");
            }
        }
        //Evento que envia el mensaje del errorprovidera null si ha escrito algo
        private void txtape_TextChanged(object sender, EventArgs e)
        {
            if (txtape.Text != "")
            {
                error.SetError(txtape, "");
            }
        }
        //Evento que envia el mensaje del errorprovider si no se ha escrito una vez cambia el foco
        private void txtnom_Leave(object sender, EventArgs e)
        {
            if (txtnom.Text == "")
            {
                error.SetError(txtnom, "Debe ingresar el nombre obligatoriamente");
            }
        }
        //Evento que envia el mensaje del errorprovidera null si ha escrito algo
        private void txtnom_TextChanged(object sender, EventArgs e)
        {
            if (txtnom.Text != "")
            {
                error.SetError(txtnom, "");
            }
        }
        //Evento que envia el mensaje del errorprovider si no se ha escrito una vez cambia el foco
        private void txtdir_Leave(object sender, EventArgs e)
        {
            if (txtdir.Text == "")
            {
                error.SetError(txtdir, "Debe ingresar la direccion obligatoriamente");
            }
        }
        //Evento que envia el mensaje del errorprovidera null si ha escrito algo
        private void txtdir_TextChanged(object sender, EventArgs e)
        {
            if (txtdir.Text != "")
            {
                error.SetError(txtdir, "");
            }
        }
        //Evento para que al correo no se le digite 2 veces el @
        private void txtcor_KeyPress(object sender, KeyPressEventArgs e)
        {
            char letra=e.KeyChar;
            if(letra == 64 && txtcor.Text.Contains('@'))
                e.Handled=true;
        }
    }
}
