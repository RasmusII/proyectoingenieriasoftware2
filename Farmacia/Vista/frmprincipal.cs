﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Farmacia.Controlador;

namespace Farmacia.Vista
{
    public partial class frmprincipal : Form
    {
        public frmprincipal()
        {
            InitializeComponent();
        }
        //Evento que valida el cierre del sistema con el boton [salir]
        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        //Evento que valida el cierre del sistema
        private void frmprincipal_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
        //Evento que muestra la pantalla Cliente
        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int num = util.mustrapantallas("C",0);
            if (num == 1)
            {
                frmcliente frmcli = new frmcliente();
                frmcli.MdiParent = this;
                frmcli.Show();
            }
        }
        //Evento que muestra la pantalla Medicamento
        private void medicamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int num = util.mustrapantallas("M",0);
            if (num==1)
            {
                //med = new frmmedicamento();
                //med.MdiParent = this;
                //med.Show();
                frmmedicamento frmmed = new frmmedicamento();
                frmmed.MdiParent = this;
                frmmed.Show();
            }
        }
        //Evento que muestra la pantalla Lote
        private void loteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int num = util.mustrapantallas("L",0);
            if (num == 1)
            {
                frmlote frmlot = new frmlote();
                frmlot.MdiParent = this;
                frmlot.Show();
            }
        }
        //Evento que muestra la pantalla Proveedor
        private void proveedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int num = util.mustrapantallas("P",0);
            if (num == 1)
            {
                frmproveedores frmpro = new frmproveedores();
                frmpro.MdiParent = this;
                frmpro.Show();
            }
        }
        //Evento que muestra la pantalla de Iniciar Sesion (confirma usuario)
        private void cambiarToolStripMenuItem_Click(object sender, EventArgs e)
        {
                Form1 f = new Form1(1);
                f.MdiParent = this;
                f.Show();
        }
        //Evento que muestra la pantalla Registrar factura
        private void registrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int num = util.mustrapantallas("F",0);
            if (num == 1)
            {
                frmfactura frmfac = new frmfactura();
                frmfac.MdiParent = this;
                frmfac.Show();
            }
        }
        //Evento que muestra la pantalla Registrar compra
        private void registrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            int num = util.mustrapantallas("Co",0);
            if (num == 1)
            {
                frmcompra frmcom = new frmcompra();
                frmcom.MdiParent = this;
                frmcom.Show();
            }
        }
        //Evento que envia a validar la informacion si hay facturas realizadas
        private void buscarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            habilitabusquedac();            
        }
        //Metodo que valida la habilitacion de la pantalla Buscar compra
        private void habilitabusquedac()
        {
            compradb objC = new compradb();
            objC.getcompra().Listacompra = objC.traecompras();
            if (objC.getcompra().Listacompra.Count == 0)
            {
                MessageBox.Show("No hay registros");
            }
            else
            {
                int num = util.mustrapantallas("BC",0);
                if (num == 1)
                {
                    frmbuscarcompra frmbuscom = new frmbuscarcompra();
                    frmbuscom.MdiParent = this;
                    frmbuscom.Show();
                }
            }
        }
        //Evento que envia a validar la informacion si hay compras realizadas
        private void buscarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            habilitabusquedaf();            
        }
        //Metodo que valida la habilitacion de la pantalla Buscar factura
        private void habilitabusquedaf()
        {
            facturadb objF = new facturadb();
            objF.getfactura().Listafactura = objF.traefactura();
            if (objF.getfactura().Listafactura.Count == 0)
            {
                MessageBox.Show("No hay registros");
            }
            else
            {
                int num = util.mustrapantallas("BF",0);
                if (num == 1)
                {
                    frmbuscarfactura frmbuscafact = new frmbuscarfactura();
                    frmbuscafact.MdiParent = this;
                    frmbuscafact.Show();
                }
            }
        }

    }
}
