﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Farmacia.Controlador;
using Farmacia.Modelo;

namespace Farmacia.Vista
{
    public partial class frmreportes : Form
    {
        public frmreportes(string numfac)
        {
            InitializeComponent();
            this.numfac = numfac;
        }

        string numfac;

        private void frmreportes_Load(object sender, EventArgs e)
        {
            llenareporte();            
        }

        private void llenareporte()
        {
            detalleclientedb objFC = new detalleclientedb();
            rptdetfac rptcli = new rptdetfac();
            rptcli.SetDataSource(objFC.traedetallefactura(numfac));
            crystalReportViewer1.ReportSource = rptcli;
        }
    }
}
