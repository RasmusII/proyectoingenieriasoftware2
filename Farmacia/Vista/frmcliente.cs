﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Farmacia.Controlador;
using Farmacia.Modelo;

namespace Farmacia.Vista
{
    public partial class frmcliente : Form
    {
        public frmcliente()
        {
            InitializeComponent();
        }

        int fila = -1;//Variable para saber que dato de la datagridview ha sido elegido
        int numcedula;//Variable que guarda el numero de cedula
        //Evento que valida la salida de la pantalla
        private void btnsalir_Click(object sender, EventArgs e)
        {
            util.mustrapantallas("C", 1);
            Close();
        }
        //Evento para mostrar la pantalla Registrar cliente y crear un nuevo cliente
        private void btnnuevo_Click(object sender, EventArgs e)
        {
            numcedula = 0;
            frmregistrarcliente frmrclient = new frmregistrarcliente(numcedula, "N");
            this.Close();
            frmrclient.ShowDialog();
        }
        //Evento que manda a llenar la tabla informacion cliente en un metodo
        private void frmcliente_Load(object sender, EventArgs e)
        {
            llenaclientes("A");
        }
        //Metodo para llenar la tabla de informacion del cliente 
        public void llenaclientes(string est)
        {
            try
            {
                dgvinfo.Rows.Clear();
                clientedb objE = new clientedb();
                if (chkdesactivados.Checked == true)
                {
                    objE.getcliente().Listacliente = objE.traeclientes("P");
                }
                else
                {
                    objE.getcliente().Listacliente = objE.traeclientes(est);
                }                
                if (objE.getcliente().Listacliente.Count == 0)
                {
                    MessageBox.Show("No hay registros");
                    btndesactivar.Enabled = false;
                    btnmodificar.Enabled = false;
                    txtced.Enabled = false;
                }
                else
                {
                    fila = 0;
                    for (int i = 0; i < objE.getcliente().Listacliente.Count; i++)
                    {
                        dgvinfo.Rows.Add(1);
                        dgvinfo.Rows[i].Cells[0].Value = objE.getcliente().Listacliente[i].Ced_cli;
                        dgvinfo.Rows[i].Cells[1].Value = objE.getcliente().Listacliente[i].Nombre;
                        dgvinfo.Rows[i].Cells[2].Value = objE.getcliente().Listacliente[i].Dir_cli;
                        dgvinfo.Rows[i].Cells[3].Value = objE.getcliente().Listacliente[i].Tel_cli;
                    }
                    btndesactivar.Enabled = true;
                    btnmodificar.Enabled = true;
                    txtced.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }
        }
        //Evento para mostrar la pantalla Registrar cliente y modificarlo
        private void btnmodificar_Click(object sender, EventArgs e)
        {
                modificar();
                frmregistrarcliente frmrclient = new frmregistrarcliente(numcedula, "M");
                this.Close();
                frmrclient.ShowDialog();
        }
        //Evento para elegir una posicion dentro  de la tabla
        private void dgvinfo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            fila = dgvinfo.CurrentRow.Index;
        }
        //Metodo para modificar el cliente
        private void modificar()
        {
            try
            {
                clientedb objC = new clientedb();
                objC.setcliente(objC.traecliente(dgvinfo.Rows[fila].Cells[0].Value.ToString()));
                if (objC.getcliente().Ced_cli.ToString() == "")
                {
                    MessageBox.Show("Registro no existe");
                }
                else
                {
                    numcedula = objC.getcliente().Ced_cli;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }
        }
        //Evento que valida la cedula y busca esta segun el estado(Activo o desactivado)
        private void btnbuscar_Click(object sender, EventArgs e)
        {
            validaced();    
        }

        private void validaced()
        {
            string res = "";
            if (txtced.Text != "")
            {
                if (txtced.TextLength == 10)
                {
                    string ce = txtced.Text.ToString();
                    char[] vectorc = ce.ToArray();
                    ce = null;
                    for (int i = 0; i < 10; i++)
                    {
                        ce += vectorc[i].ToString();
                    }
                    res = util.validacedula(ce);
                    if (res == "Invalido")
                    {
                        MessageBox.Show("El numero de cedula es incorrecto");
                        txtced.Focus();
                    }
                    else
                    {
                        if (chkdesactivados.Checked == true)
                        {
                            buscar(txtced.Text, "P");
                        }
                        else
                        {
                            buscar(txtced.Text, "A");
                        }
                        txtced.Text = null;
                    }
                }
                else
                {
                    MessageBox.Show("Verifique si esta bien ingresado el cedula");
                    txtced.Focus();
                }
            }       
        }
        //Metodo para buscar un cliente
        private void buscar(string txt, string est)
        {
            try
            {
                dgvinfo.Rows.Clear();
                clientedb objE = new clientedb();
                objE.getcliente().Listacliente = objE.buscacliente(txt, est);
                if (objE.getcliente().Listacliente.Count >= 0)
                {
                    fila = 0;
                    for (int i = 0; i < objE.getcliente().Listacliente.Count; i++)
                    {
                        dgvinfo.Rows.Add(1);
                        dgvinfo.Rows[i].Cells[0].Value = objE.getcliente().Listacliente[i].Ced_cli;
                        dgvinfo.Rows[i].Cells[1].Value = objE.getcliente().Listacliente[i].Nombre;
                        dgvinfo.Rows[i].Cells[2].Value = objE.getcliente().Listacliente[i].Dir_cli;
                        dgvinfo.Rows[i].Cells[3].Value = objE.getcliente().Listacliente[i].Tel_cli;
                    }
                }
                else
                {
                    MessageBox.Show("No existe en el registro");
                    txtced.Focus();
                    llenaclientes(est);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error de busqueda " + ex.Message);
            }
        }
        //Evento que abilita el boton [Buscar] y llena la tabla segun el checkbox
        private void txtced_TextChanged(object sender, EventArgs e)
        {
            if (txtced.Text != "")
            {
                btnbuscar.Enabled = true;
            }
            else
            {
                if (chkdesactivados.Checked == true)
                {
                    llenaclientes("P");                    
                }
                else
                {
                    llenaclientes("A");
                }
                btnbuscar.Enabled = false;
            }
        }
        //Evento que manda a un metodo a desactivar o activar al cliente
        private void btndesactivar_Click(object sender, EventArgs e)
        {
            desactivar();
        }
        //Metodo para desactivar o activar un cliente
        private void desactivar()
        {
            try
            {
                clientedb objC = new clientedb();
                int resp;
                string mensaje = "";
                string ced = dgvinfo.Rows[fila].Cells[0].Value.ToString();
                if (MessageBox.Show("Desea " + btndesactivar.Text + " a " + dgvinfo.Rows[fila].Cells[1].Value.ToString(), "Empresa", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    if (btndesactivar.Text.Equals("Desactivar"))
                    {
                        resp = objC.desactivacliente(ced, "P");
                        mensaje = "Desactivado";
                    }
                    else
                    {
                        resp = objC.desactivacliente(ced, "A");
                        mensaje = "Activado";
                        chkdesactivados.Checked = false;
                    }
                    if (resp > 0)
                    {
                        MessageBox.Show("Cliente " + mensaje);
                        llenaclientes("A");
                    }
                    else
                    {
                        MessageBox.Show("No se desactivo el cliente ");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex.Message);
            }
        }
        //Evento que verifica en que estado esta el checkbox
        private void chkdesactivados_CheckedChanged(object sender, EventArgs e)
        {
            if (chkdesactivados.Checked == true)
            {
                llenaclientes("P");
                btndesactivar.Text = "Activar";
                btnnuevo.Enabled = false;
                btnmodificar.Enabled = false;
            }
            else
            {
                btnnuevo.Enabled = true;
                btnmodificar.Enabled = true;
                btndesactivar.Enabled = true;
                llenaclientes("A");
                btndesactivar.Text = "Desactivar";
                
            }
        }
        //Evento que valida solo la entrada de numeros en el texbox de cedula
        private void txtced_KeyPress(object sender, KeyPressEventArgs e)
        {
            char letra = e.KeyChar;
            if (!char.IsDigit(letra) && letra != 13 && letra != 8)
                e.Handled = true;
        }

        private void frmcliente_FormClosed(object sender, FormClosedEventArgs e)
        {
            util.mustrapantallas("C", 1);
        }

    }
}
