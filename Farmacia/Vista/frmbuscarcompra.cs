﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Farmacia.Controlador;

namespace Farmacia.Vista
{
    public partial class frmbuscarcompra : Form
    {
        public frmbuscarcompra()
        {
            InitializeComponent();
        }

        private void frmbuscarcompra_Load(object sender, EventArgs e)
        {
            llenainfo();
        }

        private void llenainfo()
        {
            try
            {
                dgvinfo.Rows.Clear();
                compradb objC = new compradb();
                objC.getcompra().Listacompra = objC.traecompras();
                if (objC.getcompra().Listacompra.Count == 0)
                {
                    MessageBox.Show("No hay registros");
                }
                else
                {
                    for (int i = 0; i < objC.getcompra().Listacompra.Count; i++)
                    {
                        dgvinfo.Rows.Add(1);
                        dgvinfo.Rows[i].Cells[0].Value = objC.getcompra().Listacompra[i].Id_com;
                        dgvinfo.Rows[i].Cells[1].Value = objC.getcompra().Listacompra[i].Id_empr;
                        dgvinfo.Rows[i].Cells[2].Value = objC.getcompra().Listacompra[i].Num_fact;
                        dgvinfo.Rows[i].Cells[3].Value = objC.getcompra().Listacompra[i].Fec_emi;
                        dgvinfo.Rows[i].Cells[4].Value = objC.getcompra().Listacompra[i].Total;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
            }
        }

        private void txtbuscar_TextChanged(object sender, EventArgs e)
        {
            if (txtbuscar.Text != "")
            {
                int num = int.Parse(txtbuscar.Text);
                buscar(num);
            }
            else
            {
                llenainfo();
            }
        }

        private void buscar(int n)
        {
            try
            {
                dgvinfo.Rows.Clear();
                compradb objC = new compradb();
                objC.getcompra().Listacompra = objC.buscacompra(n);
                if (objC.getcompra().Listacompra.Count > 0)
                {
                    for (int i = 0; i < objC.getcompra().Listacompra.Count; i++)
                    {
                        dgvinfo.Rows.Add(1);
                        dgvinfo.Rows[i].Cells[0].Value = objC.getcompra().Listacompra[i].Id_com;
                        dgvinfo.Rows[i].Cells[1].Value = objC.getcompra().Listacompra[i].Id_empr;
                        dgvinfo.Rows[i].Cells[2].Value = objC.getcompra().Listacompra[i].Num_fact;
                        dgvinfo.Rows[i].Cells[3].Value = objC.getcompra().Listacompra[i].Fec_emi;
                        dgvinfo.Rows[i].Cells[4].Value = objC.getcompra().Listacompra[i].Total;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error de busqueda " + ex.Message);
            }
        }

        private void txtbuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            char letra = e.KeyChar;
            if (!char.IsDigit(letra) && letra != 8 && letra != 13)
                e.Handled = true;
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            util.mustrapantallas("BC", 1);
        }

        private void frmbuscarcompra_FormClosed(object sender, FormClosedEventArgs e)
        {
            util.mustrapantallas("BC", 1);
        }
    }
}
