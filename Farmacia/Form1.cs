﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Farmacia.Vista;
using Farmacia.Controlador;

namespace Farmacia
{
    public partial class Form1 : Form
    {
        public Form1(int num)
        {
            InitializeComponent();
            this.num = num;
        }

        int user;//Variable para guardar la cedula del usuario
        string p, res="";//Variables para guardar la contraseña y la informacion del boton principal de la pantalla Iniciar sesion
        int num;//Variable que recibe parametros de otras pantallas
        //Evento que valida el registro, ingreso o cambio de informacion del usuario
        private void btnacceder_Click(object sender, EventArgs e)
        {
            if (btnacceder.Text == "Registrar")
            {
                frmpassword objpass = new frmpassword(0);
                this.Hide();
                objpass.ShowDialog();
            }
            if (btnacceder.Text == "Acceder")
            {
                if (res == "Valido")
                {
                    if (txtusuario.Text == (user.ToString()) && txtpassword.Text == p)
                    {
                        if (num == 0)
                        {
                            frmprincipal frmp = new frmprincipal();
                            this.Hide();
                            frmp.Show();
                        }
                        else
                        {
                            frmpassword frmpass = new frmpassword(1);
                            this.Hide();
                            frmpass.Show();
                        }
                    }
                    else
                    {
                        if (txtusuario.Text != (user.ToString()))
                        {
                            lbluser.Text = "Usuario incorrecto";
                        }
                        else
                        {
                            lbluser.Text = "";
                        }
                        if (txtpassword.Text != p)
                        {
                            lblpass.Text = "Contraseña incorrecta";
                        }
                        else
                        {
                            lblpass.Text = "";
                        }
                    }
                }
                else
                {
                    if (txtusuario.Text == "Cedula")
                    {
                        lbluser.Text = "Debe ingresar su numero de cédula";
                    }
                    if (txtpassword.Text == "Contraseña")
                    {
                        lblpass.Text = "Debe ingresar la contraseña";
                    }
                    MessageBox.Show("Campos vacios");
                }
            }
            
        }
        //Evento que valida ya se ha registrado el usuario
        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                iniciarsesiondb objI = new iniciarsesiondb();
                objI.setlogin(objI.traelogin("A001"));
                if (objI.getlogin().Est_ad == "")
                {
                    if (num == 0)
                    {
                        MessageBox.Show("Hola Bienvenido\nEmpiece por registrarse primero");
                    }
                }
                else
                {
                    user = objI.getlogin().Ced_ad;
                    p = objI.getlogin().Log_ad;
                    txtusuario.Enabled = true;
                    txtpassword.Enabled = true;
                    btnacceder.Text = "Acceder";
                    if (num == 1)
                    {
                        this.lblin.Location = new System.Drawing.Point(200, 9);
                        this.lblin.Font = new System.Drawing.Font("Narkisim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                        this.lblin.ForeColor = System.Drawing.Color.Red;
                        lblin.Text = "Ingrese para verificar su usuario";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en los datos, " + ex.Message);
            }
        }
        //Evento que cambia el texbox Cedula a null
        private void txtusuario_Enter(object sender, EventArgs e)
        {
            if (txtusuario.Text == "Cedula")
            {
                txtusuario.Text = "";

            }
        }
        //Evento que cambia el texbox Cedula de null a Cedula si no ha ingresado datos
        private void txtusuario_Leave(object sender, EventArgs e)
        {
            if (txtusuario.Text == "")
            {
                txtusuario.Text = "Cedula";
            }
            else
            {
                res = util.validacedula(txtusuario.Text);
                if (res == "Invalido")
                {
                    MessageBox.Show("El numero de cedula no es correcto");
                    txtusuario.Focus();
                }
            }
        }
        //Evento que cambia el texbox Contraseña a null
        private void txtpassword_Enter(object sender, EventArgs e)
        {
            if (txtpassword.Text=="Contraseña")
            {
                txtpassword.Text = "";
                txtpassword.UseSystemPasswordChar = true;

            }
        }
        //Evento que cambia el texbox Contraseña de null a Contraseña si no ha ingresado datos
        private void txtpassword_Leave(object sender, EventArgs e)
        {
            if (txtpassword.Text == "")
            {
                txtpassword.Text = "Contraseña";
                txtpassword.UseSystemPasswordChar = false;
            }
        }

        private void txtusuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            char letra = e.KeyChar;
            if (!char.IsDigit(letra) && letra != 8 && letra != 13 && letra != 32)
                e.Handled = true;
        }

    }
}
