﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.Modelo
{
    public class compra
    {
        private int id_com;

        public int Id_com
        {
            get { return id_com; }
            set { id_com = value; }
        }
        private int num_fact;

        public int Num_fact
        {
            get { return num_fact; }
            set { num_fact = value; }
        }
        private int id_empr;

        public int Id_empr
        {
            get { return id_empr; }
            set { id_empr = value; }
        }
        private string fec_emi;

        public string Fec_emi
        {
            get { return fec_emi; }
            set { fec_emi = value; }
        }
        private string total;

        public string Total
        {
            get { return total; }
            set { total = value; }
        }
        private List<compra> listacompra = new List<compra>();

        internal List<compra> Listacompra
        {
            get { return listacompra; }
            set { listacompra = value; }
        }
    }
}
