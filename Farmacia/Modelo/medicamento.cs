﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.Modelo
{
    public class medicamento
    {
        private int id_med;

        public int Id_med
        {
            get { return id_med; }
            set { id_med = value; }
        }
        private int stock;

        public int Stock
        {
            get { return stock; }
            set { stock = value; }
        }
        private string nom_med;

        public string Nom_med
        {
            get { return nom_med; }
            set { nom_med = value; }
        }
        private string tipo_med;

        public string Tipo_med
        {
            get { return tipo_med; }
            set { tipo_med = value; }
        }
        private double pre_ven;

        public double Pre_ven
        {
            get { return pre_ven; }
            set { pre_ven = value; }
        }
        private string pre_com;

        public string Pre_com
        {
            get { return pre_com; }
            set { pre_com = value; }
        }
        private List<medicamento> listamed = new List<medicamento>();

        internal List<medicamento> Listamed
        {
            get { return listamed; }
            set { listamed = value; }
        }
        private string est_med;

        public string Est_med
        {
            get { return est_med; }
            set { est_med = value; }
        }
    }
}
