﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.Modelo
{
    public class repfacturacliente
    {
        private int id_cli;

        public int Id_cli
        {
            get { return id_cli; }
            set { id_cli = value; }
        }
        private string nom_cli;

        public string Nom_cli
        {
            get { return nom_cli; }
            set { nom_cli = value; }
        }
        private string ape_cli;

        public string Ape_cli
        {
            get { return ape_cli; }
            set { ape_cli = value; }
        }
        private int ced_cli;

        public int Ced_cli
        {
            get { return ced_cli; }
            set { ced_cli = value; }
        }
        private string dir_cli;

        public string Dir_cli
        {
            get { return dir_cli; }
            set { dir_cli = value; }
        }
        private string tel_cli;

        public string Tel_cli
        {
            get { return tel_cli; }
            set { tel_cli = value; }
        }
        private string cor_cli;

        public string Cor_cli
        {
            get { return cor_cli; }
            set { cor_cli = value; }
        }        
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }


        private int id_detalle;

        public int Id_detalle
        {
            get { return id_detalle; }
            set { id_detalle = value; }
        }
        private string num_fact;

        public string Num_fact
        {
            get { return num_fact; }
            set { num_fact = value; }
        }
        private int cantidad;

        public int Cantidad
        {
            get { return cantidad; }
            set { cantidad = value; }
        }
        private string des_fact;

        public string Des_fact
        {
            get { return des_fact; }
            set { des_fact = value; }
        }
        private double precio;

        public double Precio
        {
            get { return precio; }
            set { precio = value; }
        }
        private double dettotal;

        public double Dettotal
        {
            get { return dettotal; }
            set { dettotal = value; }
        }
        private double factotal;

        public double Factotal
        {
            get { return factotal; }
            set { factotal = value; }
        }
        private double subtotal;

        public double Subtotal
        {
            get { return subtotal; }
            set { subtotal = value; }
        }
        private double iva;

        public double Iva
        {
            get { return iva; }
            set { iva = value; }
        }
        private string fec_emi;

        public string Fec_emi
        {
            get { return fec_emi; }
            set { fec_emi = value; }
        }
        List<detallefactura> listadetfact = new List<detallefactura>();

        internal List<detallefactura> Listadetfact
        {
            get { return listadetfact; }
            set { listadetfact = value; }
        }

    }
}
