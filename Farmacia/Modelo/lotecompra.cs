﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.Modelo
{
    public class lotecompra
    {
        private int id_lotcom;

        public int Id_lotcom
        {
            get { return id_lotcom; }
            set { id_lotcom = value; }
        }
        private int id_compra;

        public int Id_compra
        {
            get { return id_compra; }
            set { id_compra = value; }
        }
        private string cod_lot;

        public string Cod_lot
        {
            get { return cod_lot; }
            set { cod_lot = value; }
        }
        private int cant;

        public int Cant
        {
            get { return cant; }
            set { cant = value; }
        }
        private string precio;

        public string Precio
        {
            get { return precio; }
            set { precio = value; }
        }
        private List<lotecompra> listalotecompra = new List<lotecompra>();

        internal List<lotecompra> Listalotecompra
        {
            get { return listalotecompra; }
            set { listalotecompra = value; }
        }

    }
}
