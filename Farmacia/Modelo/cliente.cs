﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.Modelo
{
    public class cliente
    {
        private int id_cli;

        public int Id_cli
        {
            get { return id_cli; }
            set { id_cli = value; }
        }
        private string nom_cli;

        public string Nom_cli
        {
            get { return nom_cli; }
            set { nom_cli = value; }
        }
        private string ape_cli;

        public string Ape_cli
        {
            get { return ape_cli; }
            set { ape_cli = value; }
        }
        private int ced_cli;

        public int Ced_cli
        {
            get { return ced_cli; }
            set { ced_cli = value; }
        }
        private string dir_cli;

        public string Dir_cli
        {
            get { return dir_cli; }
            set { dir_cli = value; }
        }
        private string tel_cli;

        public string Tel_cli
        {
            get { return tel_cli; }
            set { tel_cli = value; }
        }
        private string cor_cli;

        public string Cor_cli
        {
            get { return cor_cli; }
            set { cor_cli = value; }
        }
        private string est_cli;

        public string Est_cli
        {
            get { return est_cli; }
            set { est_cli = value; }
        }
        private List<cliente> listacliente = new List<cliente>();

        internal List<cliente> Listacliente
        {
            get { return listacliente; }
            set { listacliente = value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

    }
}
