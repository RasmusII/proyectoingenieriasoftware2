﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.Modelo
{
    public class detallefactura
    {

        private int id_detalle;

        public int Id_detalle
        {
            get { return id_detalle; }
            set { id_detalle = value; }
        }
        private string num_fact;

        public string Num_fact
        {
            get { return num_fact; }
            set { num_fact = value; }
        }
        private int cantidad;

        public int Cantidad
        {
            get { return cantidad; }
            set { cantidad = value; }
        }
        private string des_fact;

        public string Des_fact
        {
            get { return des_fact; }
            set { des_fact = value; }
        }
        private double precio;

        public double Precio
        {
            get { return precio; }
            set { precio = value; }
        }
        private double total;

        public double Total
        {
            get { return total; }
            set { total = value; }
        }
        List<detallefactura> listadetfact = new List<detallefactura>();

        internal List<detallefactura> Listadetfact
        {
            get { return listadetfact; }
            set { listadetfact = value; }
        }
    }
}
