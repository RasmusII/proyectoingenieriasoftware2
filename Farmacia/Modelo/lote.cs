﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.Modelo
{
    public class lote
    {
        private string id_lote;

        public string Id_lote
        {
            get { return id_lote; }
            set { id_lote = value; }
        }
        private string fec_ela;

        public string Fec_ela
        {
            get { return fec_ela; }
            set { fec_ela = value; }
        }
        private string fec_cad;

        public string Fec_cad
        {
            get { return fec_cad; }
            set { fec_cad = value; }
        }
        private int id_medic;

        public int Id_medic
        {
            get { return id_medic; }
            set { id_medic = value; }
        }
        private string nom_med;

        public string Nom_med
        {
            get { return nom_med; }
            set { nom_med = value; }
        }
        private List<lote> listalote = new List<lote>();

        internal List<lote> Listalote
        {
            get { return listalote; }
            set { listalote = value; }
        }

    }
}
