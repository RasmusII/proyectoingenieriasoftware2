﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.Modelo
{
    public class iniciarsesion
    {

        private int ced_ad;

        public int Ced_ad
        {
            get { return ced_ad; }
            set { ced_ad = value; }
        }
        private string log_ad;

        public string Log_ad
        {
            get { return log_ad; }
            set { log_ad = value; }
        }

        private string est_ad;

        public string Est_ad
        {
            get { return est_ad; }
            set { est_ad = value; }
        }

        private string id_ad;

        public string Id_ad
        {
            get { return id_ad; }
            set { id_ad = value; }
        }
        private string nom_ad;

        public string Nom_ad
        {
            get { return nom_ad; }
            set { nom_ad = value; }
        }
        private string ape_ad;

        public string Ape_ad
        {
            get { return ape_ad; }
            set { ape_ad = value; }
        }
        private string tel_ad;

        public string Tel_ad
        {
            get { return tel_ad; }
            set { tel_ad = value; }
        }
        private string cor_ad;

        public string Cor_ad
        {
            get { return cor_ad; }
            set { cor_ad = value; }
        }
        private string dir_ad;

        public string Dir_ad
        {
            get { return dir_ad; }
            set { dir_ad = value; }
        }
    }
}
