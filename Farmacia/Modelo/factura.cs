﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.Modelo
{
    public class factura
    {
        private string num_fact;

        public string Num_fact
        {
            get { return num_fact; }
            set { num_fact = value; }
        }
        private string id_ger;

        public string Id_ger
        {
            get { return id_ger; }
            set { id_ger = value; }
        }
        private int id_cli;

        public int Id_cli
        {
            get { return id_cli; }
            set { id_cli = value; }
        }
        private string fec_emi;

        public string Fec_emi
        {
            get { return fec_emi; }
            set { fec_emi = value; }
        }
        private double total;

        public double Total
        {
            get { return total; }
            set { total = value; }
        }
        private double subtotal;

        public double Subtotal
        {
            get { return subtotal; }
            set { subtotal = value; }
        }
        private double iva;

        public double Iva
        {
            get { return iva; }
            set { iva = value; }
        }
        private List<factura> listafatura = new List<factura>();

        internal List<factura> Listafactura
        {
            get { return listafatura; }
            set { listafatura = value; }
        }
    }
}
