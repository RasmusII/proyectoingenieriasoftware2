﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.Modelo
{
    public class proveedor
    {
        private int id_emp;

        public int Id_emp
        {
            get { return id_emp; }
            set { id_emp = value; }
        }
        private string nom_emp;

        public string Nom_emp
        {
            get { return nom_emp; }
            set { nom_emp = value; }
        }
        
        private string ruc_emp;

        public string Ruc_emp
        {
            get { return ruc_emp; }
            set { ruc_emp = value; }
        }
        private string dir_emp;

        public string Dir_emp
        {
            get { return dir_emp; }
            set { dir_emp = value; }
        }
        private string tel_emp;

        public string Tel_emp
        {
            get { return tel_emp; }
            set { tel_emp = value; }
        }
        private string cor_emp;

        public string Cor_emp
        {
            get { return cor_emp; }
            set { cor_emp = value; }
        }
        private string est_emp;

        public string Est_emp
        {
            get { return est_emp; }
            set { est_emp = value; }
        }
        private List<proveedor> listaempresa = new List<proveedor>();

        internal List<proveedor> Listaempresa
        {
            get { return listaempresa; }
            set { listaempresa = value; }
        }
        private string rep_emp;

        public string Rep_emp
        {
            get { return rep_emp; }
            set { rep_emp = value; }
        }

    }
}
