﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;
using Farmacia.Modelo;

namespace Farmacia.Controlador
{
    public class lotedb
    {
        Conexion con = new Conexion();
        lote lot = null;
        public lote getlote()
        {
            if (this.lot == null)
            {
                this.lot = new lote();
            }
            return lot;
        }

        public void setlote(lote l)
        {
            this.lot = l;
        }

        public void limpiar()
        {
            this.lot = null;
        }

        public List<lote> traelotes()
        {
            lotedb lot = null;
            List<lote> listalo = new List<lote>();
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            try
            {
                string sqlcad = "Select * from lote order by id_lote";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    lot = new lotedb();
                    lot.getlote().Id_lote = dr[0].ToString();
                    lot.getlote().Fec_ela = dr[1].ToString();
                    lot.getlote().Fec_cad = dr[2].ToString();
                    lot.getlote().Id_medic = int.Parse(dr[3].ToString());
                    listalo.Add(lot.getlote());
                }
                dr.Close();
            }
            catch (MySqlException ex)
            {
                lot = null;
                throw ex;
            }
            catch (Exception ex)
            {
                lot = null;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return listalo;
        }

        public List<lote> buscalote(string id)
        {
            lotedb lot = null;
            List<lote> listalo = new List<lote>();
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            try
            {
                string sqlcad = "Select * from lote where id_lote='" + id + "'";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    lot = new lotedb();
                    lot.getlote().Id_lote = dr[0].ToString();
                    lot.getlote().Fec_ela = dr[1].ToString();
                    lot.getlote().Fec_cad = dr[2].ToString();
                    lot.getlote().Id_medic = int.Parse(dr[3].ToString());
                    listalo.Add(lot.getlote());
                }
                dr.Close();
            }
            catch (MySqlException ex)
            {
                lot = null;
                throw ex;
            }
            catch (Exception ex)
            {
                lot = null;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return listalo;
        }

        public string traenommed(int med)
        {
            string nom="";
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            try
            {
                string sqlcad = " Select nombre_med from medicamento Where id_med=" + med + " ";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    nom = dr[0].ToString();
                }
                dr.Close();
            }
            catch (MySqlException ex)
            {
                nom = null;
                throw ex;
            }
            catch (Exception ex)
            {
                nom = null;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return nom;
        }

        public List<lote> traelotes(int lo)
        {
            lotedb lot = null;
            List<lote> listalo = new List<lote>();
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            try
            {
                string sqlcad = "Select * from lote where id_medicamento="+ lo +"";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    lot = new lotedb();
                    lot.getlote().Id_lote = dr[0].ToString();
                    lot.getlote().Fec_ela = dr[1].ToString();
                    lot.getlote().Fec_cad = dr[2].ToString();
                    lot.getlote().Id_medic = int.Parse(dr[3].ToString());
                    listalo.Add(lot.getlote());
                }
                dr.Close();
            }
            catch (MySqlException ex)
            {
                lot = null;
                throw ex;
            }
            catch (Exception ex)
            {
                lot = null;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return listalo;
        }

        public int insertalote(lote lot)
        {
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            int resp;
            try
            {
                string sqlcad = " Insert lote values('" + lot.Id_lote + "','" + lot.Fec_ela + "','" + lot.Fec_cad + "'," + lot.Id_medic + ")";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                resp = cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                resp = 0;
                throw ex;
            }
            catch (Exception ex)
            {
                resp = 0;
                throw ex;
            }
            cn.Close();
            cmd = null;
            lot = null;
            return resp;
        }

        public lote traelote(string id)
        {
            lotedb lot = null;
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            try
            {
                string sqlcad = " Select * from lote Where id_lote='" + id + "' ";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    lot = new lotedb();
                    lot.getlote().Id_lote = dr[0].ToString();
                    lot.getlote().Fec_ela = dr[1].ToString();
                    lot.getlote().Fec_cad = dr[2].ToString();
                    lot.getlote().Id_medic = int.Parse(dr[3].ToString());
                }
                dr.Close();
            }
            catch (MySqlException ex)
            {
                lot = null;
                throw ex;
            }
            catch (Exception ex)
            {
                lot = null;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return lot.getlote();
        }

        public int Actualizalote(lote lot)
        {
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            int resp;
            try
            {
                string sqlcad = "Update lote set fecha_elaboracion='" + lot.Fec_ela + "',fecha_caducidad='" + lot.Fec_cad + "',id_medicamento=" + lot.Id_medic + " where id_lote='" + lot.Id_lote + "'";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                resp = cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                resp = 0;
                throw ex;
            }
            catch (Exception ex)
            {
                resp = 0;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return resp;
        }
    }
}
