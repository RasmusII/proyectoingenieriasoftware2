﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using Farmacia.Modelo;

namespace Farmacia.Controlador
{
    public class detallefacturadb
    {

        Conexion con = new Conexion();
        detallefactura detfact = null;

        public detallefactura getdetalle()
        {
            if (this.detfact == null)
            {
                detfact = new detallefactura();
            }
            return detfact;
        }

        public void setdetalle(detallefactura det)
        {
            detfact = det;
        }

        public void limpiar()
        {
            detfact = null;
        }

        public int insertadetallefactura(detallefactura daetfac)
        {
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            int resp = 0;
            try
            {
                string sqlcad = "Insert detalle_factura values(null,'" + daetfac.Num_fact + "'," + daetfac.Cantidad + ",'" + daetfac.Des_fact + "'," + daetfac.Precio + "," + daetfac.Total + ")";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                resp = cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                resp = 0;
                throw ex;
            }
            catch (Exception ex)
            {
                resp = 0;
                throw ex;
            }
            cn.Close();
            cmd = null;
            daetfac = null;
            return resp;
        }
    }
}
