﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;
using Farmacia.Modelo;

namespace Farmacia.Controlador
{
    public class detalleclientedb
    {
        Conexion con = new Conexion();
        repfacturacliente detclient = null;

        public repfacturacliente getdetcli()
        {
            if (this.detclient == null)
            {
                detclient = new repfacturacliente();
            }
            return detclient;
        }

        public void setDTE(repfacturacliente det)
        {
            detclient = det;
        }

        public void limpiar()
        {
            detclient = null;
        }

        public List<repfacturacliente> traedetallefactura(string num)
        {
            detalleclientedb detalle = null;
            List<repfacturacliente> listafacdet = new List<repfacturacliente>();
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            try
            {
                string sqlcad = "SELECT cliente.cedula, cliente.apellido, cliente.nombre, cliente.telefono, cliente.direccion,cliente.correo, factura.fecha,factura.subtotal,factura.IVA,factura.total,factura.num_factura,detalle_factura.cantidad,detalle_factura.des_fact,detalle_factura.precio, detalle_factura.total FROM factura INNER JOIN detalle_factura ON factura.num_factura=detalle_factura.num_factura INNER JOIN cliente ON factura.id_cliente=cliente.id_cliente WHERE factura.num_factura='"+num+"'";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    detalle = new detalleclientedb();
                    detalle.getdetcli().Ced_cli = int.Parse(dr[0].ToString());
                    detalle.getdetcli().Ape_cli = dr[1].ToString();
                    detalle.getdetcli().Nom_cli = dr[2].ToString();
                    detalle.getdetcli().Nombre = detalle.getdetcli().Ape_cli + " " + detalle.getdetcli().Nom_cli;
                    detalle.getdetcli().Tel_cli = dr[3].ToString();
                    detalle.getdetcli().Dir_cli = dr[4].ToString();
                    detalle.getdetcli().Cor_cli = dr[5].ToString();
                    detalle.getdetcli().Fec_emi = dr[6].ToString();

                    detalle.getdetcli().Subtotal = Convert.ToDouble(dr[7].ToString());
                    detalle.getdetcli().Iva = Convert.ToDouble(dr[8].ToString());
                    detalle.getdetcli().Factotal = Convert.ToDouble(dr[9].ToString());

                    detalle.getdetcli().Num_fact = dr[10].ToString();
                    detalle.getdetcli().Cantidad = int.Parse(dr[11].ToString());
                    detalle.getdetcli().Des_fact = dr[12].ToString();
                    detalle.getdetcli().Precio = Convert.ToDouble(dr[13].ToString());
                    detalle.getdetcli().Dettotal = Convert.ToDouble(dr[14].ToString());
                    listafacdet.Add(detalle.getdetcli());
                }
                dr.Close();
            }
            catch (MySqlException ex)
            {
                detalle = null;
                throw ex;
            }
            catch (Exception ex)
            {
                detalle = null;
                throw ex;
            }
            cn.Close();
            cmd = null;
            detalle = null;
            return listafacdet;
        }
    }
}
