﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using Farmacia.Modelo;

namespace Farmacia.Controlador
{
    public class lotecompradb
    {
        Conexion con = new Conexion();
        lotecompra locom = null;

        public lotecompra getlotecom()
        {
            if (this.locom == null)
            {
                locom = new lotecompra();
            }
            return locom;
        }

        public void setlotecom(lotecompra loc)
        {
            locom = loc;
        }

        public void limpiar()
        {
            locom = null;
        }

        public int insertalotecompra(lotecompra lotcom)
        {
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            int resp = 0;
            try
            {
                string sqlcad = "Insert lote_compra values(null," + lotcom.Id_compra + ",'" + lotcom.Cod_lot + "',"+ lotcom.Cant +","+ lotcom.Precio +")";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                resp = cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                resp = 0;
                throw ex;
            }
            catch (Exception ex)
            {
                resp = 0;
                throw ex;
            }
            cn.Close();
            cmd = null;
            lotcom = null;
            return resp;
        }
    }
}
