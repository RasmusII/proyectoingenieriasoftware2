﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using Farmacia.Modelo;

namespace Farmacia.Controlador
{
    public class facturadb
    {
        Conexion con = new Conexion();
        factura fact = null;

        public factura getfactura()
        {
            if (this.fact == null)
            {
                fact = new factura();
            }
            return fact;
        }

        public void setfactura(factura fa)
        {
            fact = fa;
        }

        public void limpiar()
        {
            fact = null;
        }

        public int traecodigo()
        {
            int nro = 0;
            MySqlConnection cn = con.GetConnection();
            MySqlCommand cmd;
            try
            {
                string sqlcad = "Select max(num_factura) as nro from factura";
                cmd = new MySqlCommand(sqlcad, cn);
                cn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    if (DBNull.Value == dr["nro"])
                        nro = 0;
                    else
                        nro = Convert.ToInt32(dr["nro"]);
                }
                dr.Close();
            }
            catch (MySqlException ex)
            {
                nro = 0;
                throw ex;
            }
            catch (Exception ex)
            {
                nro = 0;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return nro;
        }

        public int actualizastock(int cant, string opc, string nom)
        {
            string sqlcad = "";
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            int resp;
            try
            {
                if (opc == "D")
                {
                    sqlcad = "Update medicamento set stock =" + cant + " where nombre_med ='" + nom + "'";
                }
                else
                {
                    sqlcad = "Update medicamento set stock=stock+" + cant + " where nombre_med ='" + nom + "'";
                }
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                resp = cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                resp = 0;
                throw ex;
            }
            catch (Exception ex)
            {
                resp = 0;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return resp;
        }

        public int insertafactura(factura fac)
        {
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            int resp;
            try
            {
                string sqlcad = " Insert factura values('" + fac.Num_fact + "','" + fac.Fec_emi + "'," + fac.Id_cli + "," + fac.Subtotal + "," + fac.Iva + "," + fac.Total + ",'" + fac.Id_ger + "')";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                resp = cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                resp = 0;
                throw ex;
            }
            catch (Exception ex)
            {
                resp = 0;
                throw ex;
            }
            cn.Close();
            cmd = null;
            fac = null;
            return resp;
        }

        public List<factura> traefactura()
        {
            facturadb fac = null;
            List<factura> listafac = new List<factura>();
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            try
            {
                string sqlcad = "Select * from factura order by num_factura";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    fac = new facturadb();
                    fac.getfactura().Num_fact = dr[0].ToString();
                    fac.getfactura().Fec_emi = dr[1].ToString();
                    fac.getfactura().Id_cli = int.Parse(dr[2].ToString());
                    fac.getfactura().Subtotal = Convert.ToDouble(dr[3].ToString());
                    fac.getfactura().Iva = Convert.ToDouble(dr[4].ToString());
                    fac.getfactura().Total = Convert.ToDouble(dr[5].ToString());
                    listafac.Add(fac.getfactura());
                }
                dr.Close();
            }
            catch (MySqlException ex)
            {
                fac = null;
                throw ex;
            }
            catch (Exception ex)
            {
                fac = null;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return listafac;
        }

        public List<factura> buscafactura(int n)
        {
            facturadb fac = null;
            List<factura> listafac = new List<factura>();
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            try
            {
                string sqlcad = "Select * from factura where num_factura=" + n + "";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    fac = new facturadb();
                    fac.getfactura().Num_fact = dr[0].ToString();
                    fac.getfactura().Fec_emi = dr[1].ToString();
                    fac.getfactura().Id_cli = int.Parse(dr[2].ToString());
                    fac.getfactura().Total = Convert.ToDouble(dr[3].ToString());
                    listafac.Add(fac.getfactura());
                }
                dr.Close();
            }
            catch (MySqlException ex)
            {
                fac = null;
                throw ex;
            }
            catch (Exception ex)
            {
                fac = null;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return listafac;
        }
    }
}
