﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;
using Farmacia.Modelo;

namespace Farmacia.Controlador
{
    public class medicamentodb
    {
        Conexion con = new Conexion();
        medicamento med = null;
        public medicamento getmed()
        {
            if (this.med == null)
            {
                this.med = new medicamento();
            }
            return med;
        }

        public void setmed(medicamento medic)
        {
            this.med = medic;
        }

        public void limpiar()
        {
            this.med = null;
        }

        public List<medicamento> buscamed(string txt, string est)
        {
            string sqlcad;
            medicamentodb med = null;
            List<medicamento> listamed = new List<medicamento>();
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            try
            {
                if (est == "M")
                {
                    sqlcad = "Select * from medicamento where nombre_med='" + txt + "' ";
                }
                else
                {
                    sqlcad = "Select * from medicamento where nombre_med like '" + txt + "%' and est_med='" + est + "' order by nombre_med";
                }
                    cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    med = new medicamentodb();
                    med.getmed().Id_med = int.Parse(dr[0].ToString());
                    med.getmed().Stock = int.Parse(dr[1].ToString());
                    med.getmed().Nom_med = dr[2].ToString();
                    med.getmed().Tipo_med = dr[3].ToString();
                    med.getmed().Pre_ven = Convert.ToDouble(dr[4].ToString());
                    med.getmed().Pre_com = dr[5].ToString();
                    med.getmed().Est_med = dr[6].ToString();
                    listamed.Add(med.getmed());
                }
                dr.Close();
            }
            catch (MySqlException ex)
            {
                med = null;
                throw ex;
            }
            catch (Exception ex)
            {
                med = null;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return listamed;
        }

        public List<medicamento> traemed(string est)
        {
            medicamentodb med = null;
            List<medicamento> listamed = new List<medicamento>();
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            try
            {
                string sqlcad = "Select * from medicamento Where est_med='" + est + "' order by nombre_med";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    med = new medicamentodb();
                    med.getmed().Id_med = int.Parse(dr[0].ToString());
                    med.getmed().Stock = int.Parse(dr[1].ToString());
                    med.getmed().Nom_med = dr[2].ToString();
                    med.getmed().Tipo_med = dr[3].ToString();
                    med.getmed().Pre_ven = Convert.ToDouble(dr[4].ToString());
                    med.getmed().Pre_com = dr[5].ToString();
                    listamed.Add(med.getmed());
                }
                dr.Close();
            }
            catch (MySqlException ex)
            {
                med = null;
                throw ex;
            }
            catch (Exception ex)
            {
                med = null;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return listamed;
        }

        public int insertamed(medicamento med)
        {
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            int resp;
            try
            {
                string sqlcad = " Insert medicamento values(null,0,'" + med.Nom_med + "','" + med.Tipo_med + "'," + med.Pre_ven + ",0,'" + med.Est_med + "')";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                resp = cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                resp = 0;
                throw ex;
            }
            catch (Exception ex)
            {
                resp = 0;
                throw ex;
            }
            cn.Close();
            cmd = null;
            med = null;
            return resp;
        }

        public medicamento traemedic(string id_med)
        {
            medicamentodb med = null;
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            try
            {
                string sqlcad = " Select * from medicamento Where id_med=" + id_med + " ";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    med = new medicamentodb();
                    med.getmed().Id_med = int.Parse(dr[0].ToString());
                    med.getmed().Stock = int.Parse(dr[1].ToString());
                    med.getmed().Nom_med = dr[2].ToString();
                    med.getmed().Tipo_med = dr[3].ToString();
                    med.getmed().Pre_ven = Convert.ToDouble(dr[4].ToString());
                    med.getmed().Pre_com = dr[5].ToString();
                }
                dr.Close();
            }
            catch (MySqlException ex)
            {
                med = null;
                throw ex;
            }
            catch (Exception ex)
            {
                med = null;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return med.getmed();
        }

        public int Actualizamed(medicamento med)
        {
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            int resp;
            try
            {
                string sqlcad = "Update medicamento set precio_venta =" + med.Pre_ven + " where id_med=" + med.Id_med + "";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                resp = cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                resp = 0;
                throw ex;
            }
            catch (Exception ex)
            {
                resp = 0;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return resp;
        }

        public int desactivamed(string id, string est)
        {
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            int resp = 0;
            try
            {
                string sqlcad = "Update medicamento set est_med='" + est + "' where id_med=" + id + "";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                resp = cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                resp = 0;
                throw ex;
            }
            catch (Exception ex)
            {
                resp = 0;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return resp;
        }

        //public List<medicamento> traecuentacbo()
        //{
        //    medicamento med = null;
        //    List<medicamento> listam = new List<medicamento>();
        //    MySqlCommand cmd;
        //    MySqlConnection cn = con.GetConnection();
        //    try
        //    {
        //        string sqlcad = "Select * from medicamento where est_med='A' order by nombre_med";
        //        cmd = new MySqlCommand(sqlcad, cn);
        //        cmd.CommandType = CommandType.Text;
        //        cn.Open();
        //        MySqlDataReader dr = cmd.ExecuteReader();
        //        while (dr.Read())
        //        {
        //            med = new medicamento();
        //            med.Id_med = int.Parse(dr["id_med"].ToString());
        //            med.Stock = int.Parse(dr["stock"].ToString());
        //            med.Nom_med = dr["nombre_med"].ToString();
        //            med.Tipo_med = dr["tipo_med"].ToString();
        //            med.Pre_ven = Convert.ToDouble(dr["precio_venta"].ToString());
        //            med.Pre_com = dr["precio_compra"].ToString();
        //            med.Est_med = dr["est_med"].ToString();                    
        //            listam.Add(med);
        //        }
        //        dr.Close();
        //    }
        //    catch (MySqlException ex)
        //    {
        //        med = null;
        //        throw ex;
        //    }
        //    catch (Exception ex)
        //    {
        //        med = null;
        //        throw ex;
        //    }
        //    cn.Close();
        //    cmd = null;
        //    return listam;
        //}

        public int insertastock(medicamento stock)
        {
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            int resp;
            try
            {
                string sqlcad = "Update medicamento set stock=stock+" + stock.Stock + ", precio_compra="+ stock.Pre_com +" where nombre_med='" + stock.Nom_med + "'";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                resp = cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                resp = 0;
                throw ex;
            }
            catch (Exception ex)
            {
                resp = 0;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return resp;
        }
    }
}
