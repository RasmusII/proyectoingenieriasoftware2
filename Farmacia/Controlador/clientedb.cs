﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;
using Farmacia.Modelo;

namespace Farmacia.Controlador
{
    public class clientedb
    {
        Conexion con = new Conexion();
        cliente client = null;

        public cliente getcliente()
        {
            if (this.client == null)
            {
                this.client = new cliente();
            }
            return client;
        }

        public void setcliente(cliente cli)
        {
            this.client = cli;
        }
        
        public int insertacliente(cliente cli)
        {
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            int resp;
            try
            {
                string sqlcad = "Insert cliente values (null,'" + cli.Nom_cli + "','" + cli.Ape_cli + "'," + cli.Ced_cli + ",'" + cli.Dir_cli + "','" + cli.Tel_cli + "','" + cli.Cor_cli + "','" + cli.Est_cli + "')";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                resp = cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                resp = 0;
                throw ex;
            }
            catch (Exception ex)
            {
                resp = 0;
                throw ex;
            }
            cn.Close();
            cmd = null;
            cli = null;
            return resp;
        }
       
        public List<cliente> traeclientes(string est)
        {
            clientedb cli = null;
            List<cliente> listaemp = new List<cliente>();
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            try
            {
                string sqlcad = "Select * from cliente Where est_cli='" + est + "' order by apellido";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    cli = new clientedb();
                    cli.getcliente().Id_cli = int.Parse(dr[0].ToString());
                    cli.getcliente().Nom_cli = dr[1].ToString();
                    cli.getcliente().Ape_cli = dr[2].ToString();
                    cli.getcliente().Ced_cli = int.Parse(dr[3].ToString());
                    cli.getcliente().Dir_cli = dr[4].ToString();
                    cli.getcliente().Tel_cli = dr[5].ToString();
                    cli.getcliente().Cor_cli = dr[6].ToString();
                    cli.getcliente().Est_cli = dr[7].ToString();
                    cli.getcliente().Nombre = cli.getcliente().Ape_cli + " " + cli.getcliente().Nom_cli;
                    listaemp.Add(cli.getcliente());
                }
                dr.Close();
            }
            catch (MySqlException ex)
            {
                cli = null;
                throw ex;
            }
            catch (Exception ex)
            {
                cli = null;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return listaemp;
        }
        
        public cliente traecliente(string ced)
        {
            clientedb cli = null;
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            try
            {
                string sqlcad = " Select * from cliente Where cedula='" + ced + "' ";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    cli = new clientedb();
                    cli.getcliente().Id_cli = int.Parse(dr[0].ToString());
                    cli.getcliente().Nom_cli = dr[1].ToString();
                    cli.getcliente().Ape_cli = dr[2].ToString();
                    cli.getcliente().Ced_cli = int.Parse(dr[3].ToString());
                    cli.getcliente().Dir_cli = dr[4].ToString();
                    cli.getcliente().Tel_cli = dr[5].ToString();
                    cli.getcliente().Cor_cli = dr[6].ToString();
                    cli.getcliente().Est_cli = dr[7].ToString();
                }
                dr.Close();
            }
            catch (MySqlException ex)
            {
                cli = null;
                throw ex;
            }
            catch (Exception ex)
            {
                cli = null;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return cli.getcliente(); 
        }

        public int Actualizacliente(cliente cli)
        {
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            int resp;
            try
            {
                string sqlcad = "Update cliente set nombre='" + cli.Nom_cli + "',apellido='"+cli.Ape_cli+"',direccion='" + cli.Dir_cli + "',telefono='" + cli.Tel_cli + "',correo='" + cli.Cor_cli + "',est_cli='" + cli.Est_cli + "' where cedula='" + cli.Ced_cli + "'";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                resp = cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                resp = 0;
                throw ex;
            }
            catch (Exception ex)
            {
                resp = 0;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return resp;
        }

        public List<cliente> buscacliente(string txt, string est)
        {
            string sqlcad="";
            clientedb cli = null;
            List<cliente> listaemp = new List<cliente>();
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            try
            {
                if (est == "T")
                {
                    sqlcad = "Select * from cliente where cedula = " + txt + "";
                }
                else
                {
                    if (est == "O")
                    {
                        sqlcad = "Select * from cliente where id_cliente = " + txt + "";
                    }
                    else
                    {
                        sqlcad = "Select * from cliente where cedula = " + txt + " and est_cli='" + est + "'";
                    }
                }
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    cli = new clientedb();
                    cli.getcliente().Id_cli = int.Parse(dr[0].ToString());
                    cli.getcliente().Nom_cli = dr[1].ToString();
                    cli.getcliente().Ape_cli = dr[2].ToString();
                    cli.getcliente().Ced_cli = int.Parse(dr[3].ToString());
                    cli.getcliente().Dir_cli = dr[4].ToString();
                    cli.getcliente().Tel_cli = dr[5].ToString();
                    cli.getcliente().Cor_cli = dr[6].ToString();
                    cli.getcliente().Est_cli = dr[7].ToString();
                    cli.getcliente().Nombre = cli.getcliente().Ape_cli + " " + cli.getcliente().Nom_cli;
                    listaemp.Add(cli.getcliente());
                }
                dr.Close();
            }
            catch (MySqlException ex)
            {
                cli = null;
                throw ex;
            }
            catch (Exception ex)
            {
                cli = null;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return listaemp;
        }

        public int desactivacliente(string ced, string est)
        {
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            int resp = 0;
            try
            {
                string sqlcad = "Update cliente set est_cli='" + est + "' where cedula='" + ced + "'";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                resp = cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                resp = 0;
                throw ex;
            }
            catch (Exception ex)
            {
                resp = 0;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return resp;
        }
    }
}
