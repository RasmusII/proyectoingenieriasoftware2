﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.Controlador
{
    class util
    {
        public static int med = 0, cli = 0, pro = 0, com = 0, fac = 0, lot = 0,bc=0,bf=0;
        public static int mustrapantallas(string f, int n)
        {
            int opc = 0;
            if (n == 0)
            {
                if (f == "M" && med == 0)
                {
                    med = 1; opc = 1;
                }
                if (f == "C" && cli == 0)
                {
                    cli = 1; opc = 1;
                }
                if (f == "P" && pro == 0)
                {
                    pro = 1; opc = 1;
                }
                if (f == "Co" && com == 0)
                {
                    com = 1; opc = 1;
                }
                if (f == "F" && fac < 3)
                {
                    fac++; opc = 1;
                }
                if (f == "L" && lot == 0)
                {
                    lot = 1; opc = 1;
                }
                if (f == "BF" && lot == 0)
                {
                    bf = 1; opc = 1;
                }
                if (f == "BC" && lot == 0)
                {
                    bc = 1; opc = 1;
                } 
            }
            else
            {
                if(f=="M")
                    med = 0;
                if (f == "C")
                    cli = 0;
                if (f == "P") 
                    pro = 0;
                if (f == "Co") 
                    com = 0;
                if (f == "F") 
                    fac--;
                if (f == "L") 
                    lot = 0;
                if (f == "BF")
                    bf=0;
                if (f == "BC")
                    bc = 0;
            }
            return opc;
        }

        public static string girafecha(string f)
        {
            string fec = "";
            fec = f.Substring(6, 4) + "-" + f.Substring(3, 2) + "-" + f.Substring(0, 2);
            return fec;
        }

        public static string validacedula(string cedula)
        {
            int suma = 0, num, aux, aux2 = 0;
            char[] vectorc = cedula.ToArray();
            for (int i = 0; i < cedula.Length ; i++)
            {
                aux = Convert.ToInt32(vectorc[i].ToString());
                if (i == cedula.Length - 1)
                {
                    aux2 = aux;
                    continue;
                }

                if (i % 2 == 0)
                {
                    num = aux * 2;
                    if (num > 9)
                    {
                        suma += num - 9;
                    }
                    else
                    {
                        suma += num;
                    }
                }
                else
                {
                    suma += aux;
                }
            }

            int cedu = suma % 10;
            int ced1 = suma / 10;
            if (ced1 > 0 && cedu > 0)
            {
                ced1 = (ced1 + 1) * 10;
            }
            else
            {
                ced1 = ced1 * 10;
            }
            if ((ced1 - suma) != aux2)
            {
                return "Invalido";
            }
            else
            {
                return "Valido";
            }
        }

        public static string validacorreo(string correo)
        {
            char[] del = {'@'};
            string[] vcorreo = correo.Split(del);
            if (vcorreo[0].Length > 0 && vcorreo[1].Length > 0) 
            {
                return "Valido";
            }
            else
            {
                return "Invalido";
            }
        }

        public static string numerofac(int nro)
        {
            string n = nro.ToString();
            string num = "";
            
            for (int i = 0; i < (9-n.Length); i++)
            {
                num += "0";
            }
            num += nro;
            return num;
        }        
    }    
}
