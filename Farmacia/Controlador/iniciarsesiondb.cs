﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;
using Farmacia.Modelo;

namespace Farmacia.Controlador
{
    public class iniciarsesiondb
    {
        Conexion con = new Conexion();
        iniciarsesion login;

        public iniciarsesion getlogin()
        {
            if (this.login == null)
            {
                login = new iniciarsesion();
            }
            return login;
        }

        public void setlogin(iniciarsesion log)
        {
            login = log;
        }

        public iniciarsesion traelogin(string i)
        {
            int aux=0;
            iniciarsesiondb iniciar = null;
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            try
            {
                string sqlcad = "Select * from gerente Where id_ger='" + i + "'";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    iniciar = new iniciarsesiondb();
                    iniciar.getlogin().Id_ad = dr[0].ToString();
                    iniciar.getlogin().Nom_ad = dr[1].ToString();
                    iniciar.getlogin().Ape_ad = dr[2].ToString();
                    iniciar.getlogin().Tel_ad = dr[3].ToString();
                    iniciar.getlogin().Ced_ad = int.Parse(dr[4].ToString());
                    iniciar.getlogin().Log_ad = dr[5].ToString();
                    iniciar.getlogin().Est_ad = dr[6].ToString();
                    aux = 1;
                }
                if (aux==0)
                {
                    iniciar = new iniciarsesiondb();
                    iniciar.getlogin().Est_ad = "";
                }
                dr.Close();
            }
            catch (MySqlException ex)
            {
                iniciar = null;
                throw ex;
            }
            catch (Exception ex)
            {
                iniciar = null;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return iniciar.getlogin();
        }

        public int insertaadmin(iniciarsesion ad, int num)
        {
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            int resp;
            string sqlcad;
            try
            {
                if (num == 0)
                {
                    sqlcad = " Insert gerente values('" + ad.Id_ad + "','" + ad.Nom_ad + "','" + ad.Ape_ad + "'," + ad.Tel_ad + "," + ad.Ced_ad + ",'" + ad.Log_ad + "','" + ad.Est_ad + "')";
                }
                else
                {
                    sqlcad = "Update gerente set telefono='"+ad.Tel_ad+"',contraseña=" + ad.Log_ad + " where id_ger='A001'";
                }
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                resp = cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                resp = 0;
                throw ex;
            }
            catch (Exception ex)
            {
                resp = 0;
                throw ex;
            }
            cn.Close();
            cmd = null;
            ad = null;
            return resp;
        }

    }
}
