﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using Farmacia.Modelo;

namespace Farmacia.Controlador
{
    public class compradb
    {
        Conexion con = new Conexion();
        compra comp = null;

        public compra getcompra()
        {
            if (this.comp == null)
            {
                comp = new compra();
            }
            return comp;
        }

        public void setcompra(compra com)
        {
            comp = com;
        }

        public void limpiar()
        {
            comp = null;
        }

        public int generacodigo()
        {
            int nro = 0;
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            try
            {
                string sqlcad = "Select Max(id_compra) as nro from compra";
                cmd = new MySqlCommand(sqlcad, cn);
                cn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    if (DBNull.Value == dr["nro"])
                    {
                        nro = 0;
                    }
                    else
                    {
                        nro = Convert.ToInt32(dr["nro"]);
                    }
                }
                dr.Close();
            }
            catch (MySqlException ex)
            {
                nro = 0;
                throw ex;
            }
            catch (Exception ex)
            {
                nro = 0;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return nro;
        }

        public int insertacompra(compra com)
        {
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            int resp;
            try
            {
                string sqlcad = " Insert compra values("+ com.Id_com +"," + com.Id_empr + "," + com.Num_fact + ",'" + com.Fec_emi + "'," + com.Total + ")";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                resp = cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                resp = 0;
                throw ex;
            }
            catch (Exception ex)
            {
                resp = 0;
                throw ex;
            }
            cn.Close();
            cmd = null;
            com = null;
            return resp;
        }

        public List<compra> traecompras()
        {
            compradb com = null;
            List<compra> listaemp = new List<compra>();
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            try
            {
                string sqlcad = "Select * from compra order by id_compra";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    com = new compradb();
                    com.getcompra().Id_com = int.Parse(dr[0].ToString());
                    com.getcompra().Id_empr = int.Parse(dr[1].ToString());
                    com.getcompra().Num_fact = int.Parse(dr[2].ToString());
                    com.getcompra().Fec_emi = dr[3].ToString();
                    com.getcompra().Total = dr[4].ToString();
                    listaemp.Add(com.getcompra());
                }
                dr.Close();
            }
            catch (MySqlException ex)
            {
                com = null;
                throw ex;
            }
            catch (Exception ex)
            {
                com = null;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return listaemp;
        }

        public List<compra> buscacompra(int n)
        {
            compradb com = null;
            List<compra> listaemp = new List<compra>();
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            try
            {
                string sqlcad = "Select * from compra where id_compra="+ n +"";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    com = new compradb();
                    com.getcompra().Id_com = int.Parse(dr[0].ToString());
                    com.getcompra().Id_empr = int.Parse(dr[1].ToString());
                    com.getcompra().Num_fact = int.Parse(dr[2].ToString());
                    com.getcompra().Fec_emi = dr[3].ToString();
                    com.getcompra().Total = dr[4].ToString();
                    listaemp.Add(com.getcompra());
                }
                dr.Close();
            }
            catch (MySqlException ex)
            {
                com = null;
                throw ex;
            }
            catch (Exception ex)
            {
                com = null;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return listaemp;
        }

        public int traenumfac(int n, string numfact)
        {
            compradb com = null;
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            int resp = 0;
            try
            {
                string sqlcad = "Select * from compra where id_emp=" + n + " and num_fact="+ numfact+"";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    com = new compradb();
                    com.getcompra().Id_com = int.Parse(dr[0].ToString());
                    com.getcompra().Id_empr = int.Parse(dr[1].ToString());
                    com.getcompra().Num_fact = int.Parse(dr[2].ToString());
                    com.getcompra().Fec_emi = dr[3].ToString();
                    com.getcompra().Total = dr[4].ToString();
                    resp = 1;
                }
                dr.Close();
            }
            catch (MySqlException ex)
            {
                com = null;
                throw ex;
            }
            catch (Exception ex)
            {
                com = null;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return resp;
        }
    }
}
