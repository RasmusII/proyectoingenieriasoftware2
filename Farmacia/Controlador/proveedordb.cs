﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;
using Farmacia.Modelo;

namespace Farmacia.Controlador
{
    public class proveedordb
    {
        Conexion con = new Conexion();
        proveedor prov = null;

        public proveedor getemp()
        {
            if (this.prov == null)
            {
                this.prov = new proveedor();
            }
            return prov;
        }

        public void setemp(proveedor p)
        {
            this.prov = p;
        }

        public int insertaemp(proveedor emp)
        {
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            int resp;
            try
            {
                string sqlcad = "Insert proveedor values (null,'" + emp.Nom_emp + "','" + emp.Tel_emp + "','" + emp.Ruc_emp + "','" + emp.Dir_emp + "','" + emp.Cor_emp + "','" + emp.Rep_emp + "','" + emp.Est_emp + "')";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                resp = cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                resp = 0;
                throw ex;
            }
            catch (Exception ex)
            {
                resp = 0;
                throw ex;
            }
            cn.Close();
            cmd = null;
            emp = null;
            return resp;
        }

        public int Actualizaemp(proveedor emp)
        {
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            int resp;
            try
            {
                string sqlcad = "Update proveedor set nom_emp='" + emp.Nom_emp + "',tel_emp='" + emp.Tel_emp + "',dir_emp='" + emp.Dir_emp + "',cor_emp='" + emp.Cor_emp + "',representante='" + emp.Rep_emp + "',est_emp='" + emp.Est_emp + "' where ruc_emp='" + emp.Ruc_emp + "' ";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                resp = cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                resp = 0;
                throw ex;
            }
            catch (Exception ex)
            {
                resp = 0;
                throw ex;
            }
            cn.Close();
            cmd = null;
            emp = null;
            return resp;
        }

        public List<proveedor> traedatos(string est)
        {
            proveedordb emp = null;
            List<proveedor> listaemp = new List<proveedor>();
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            try
            {
                string sqlcad = "Select * from proveedor Where est_emp='" + est + "' order by nom_emp";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    emp = new proveedordb();
                    emp.getemp().Id_emp = int.Parse(dr[0].ToString());
                    emp.getemp().Nom_emp = dr[1].ToString();
                    emp.getemp().Tel_emp = dr[2].ToString();
                    emp.getemp().Ruc_emp = dr[3].ToString();
                    emp.getemp().Dir_emp = dr[4].ToString();
                    emp.getemp().Cor_emp = dr[5].ToString();
                    emp.getemp().Rep_emp = dr[6].ToString();
                    emp.getemp().Est_emp = dr[7].ToString();
                    listaemp.Add(emp.getemp());
                }
                dr.Close();
            }
            catch (MySqlException ex)
            {
                emp = null;
                throw ex;
            }
            catch (Exception ex)
            {
                emp = null;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return listaemp;
        }

        public proveedor traeemp(string ruc)
        {
            proveedordb emp = null;
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            try
            {
                string sqlcad = " Select * from proveedor Where ruc_emp='" + ruc + "' ";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    emp = new proveedordb();
                    emp.getemp().Id_emp = int.Parse(dr[0].ToString());
                    emp.getemp().Nom_emp = dr[1].ToString();
                    emp.getemp().Tel_emp = dr[2].ToString();
                    emp.getemp().Ruc_emp = dr[3].ToString();
                    emp.getemp().Dir_emp = dr[4].ToString();
                    emp.getemp().Cor_emp = dr[5].ToString();
                    emp.getemp().Rep_emp = dr[6].ToString();
                    emp.getemp().Est_emp = dr[7].ToString();
                }
                dr.Close();
            }
            catch (MySqlException ex)
            {
                emp = null;
                throw ex;
            }
            catch (Exception ex)
            {
                emp = null;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return emp.getemp(); ;
        }

        public proveedor traecodigo(string nom)
        {
            proveedordb emp = null;
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            try
            {
                string sqlcad = " Select id_emp from proveedor Where nom_emp='" + nom + "' ";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    emp = new proveedordb();
                    emp.getemp().Id_emp = int.Parse(dr[0].ToString());                    
                }
                dr.Close();
            }
            catch (MySqlException ex)
            {
                emp = null;
                throw ex;
            }
            catch (Exception ex)
            {
                emp = null;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return emp.getemp(); ;
        }

        public List<proveedor> buscaemp(string txt, string est)
        {
            string sqlcad;
            proveedordb emp = null;
            List<proveedor> listaemp = new List<proveedor>();
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            try
            {
                if (est == "T")
                {
                    sqlcad = "Select * from proveedor where ruc_emp = " + txt + "";
                }
                else
                {
                    sqlcad = "Select * from proveedor where ruc_emp = " + txt + " and est_emp='" + est + "'";
                }
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    emp = new proveedordb();
                    emp.getemp().Id_emp = int.Parse(dr[0].ToString());
                    emp.getemp().Nom_emp = dr[1].ToString();
                    emp.getemp().Tel_emp = dr[2].ToString();
                    emp.getemp().Ruc_emp = dr[3].ToString();
                    emp.getemp().Dir_emp = dr[4].ToString();
                    emp.getemp().Cor_emp = dr[5].ToString();
                    emp.getemp().Rep_emp = dr[6].ToString();
                    emp.getemp().Est_emp = dr[7].ToString();
                    listaemp.Add(emp.getemp());
                }
                dr.Close();
            }
            catch (MySqlException ex)
            {
                emp = null;
                throw ex;
            }
            catch (Exception ex)
            {
                emp = null;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return listaemp;
        }

        public int desactivaemp(string ced, string est)
        {
            MySqlCommand cmd;
            MySqlConnection cn = con.GetConnection();
            int resp = 0;
            try
            {
                string sqlcad = "Update proveedor set est_emp='" + est + "' where ruc_emp='" + ced + "'";
                cmd = new MySqlCommand(sqlcad, cn);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                resp = cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                resp = 0;
                throw ex;
            }
            catch (Exception ex)
            {
                resp = 0;
                throw ex;
            }
            cn.Close();
            cmd = null;
            return resp;
        }
    }
}
