﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Farmacia
{
    public class Conexion
    {

        //string connectionString = "Database=empresa; Data Source = localhost;User Id=proyecto;Password=proyecto";//Variable para realizar la conexion a la base de datos
        static string conexion = "SERVER=127.0.0.1; PORT=3306; DATABASE=empresa; UID=proyecto; PASSWORD=proyecto";
        //Metodo para conectar a la base de datos
        public MySqlConnection GetConnection()
        {
            MySqlConnection objcon = new MySqlConnection(conexion);
            return objcon;
        }
    }
}
